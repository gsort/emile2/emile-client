require 'firebase_cloud_messenger'

FirebaseCloudMessenger.credentials_path = ENV["FIREBASE_SVCACCOUNT_FILE"]

message = {
  notification: {
    title: "YEAH!",
    body: "Testing from Ruby!"
  },
  topic: "weather"
}

puts "Message is valid? #{FirebaseCloudMessenger.validate_message(message)}"

begin
  FirebaseCloudMessenger.send(message: message)
rescue FirebaseCloudMessenger::Error => e
  e.class # => FirebaseCloudMessenger::BadRequest
  e.short_message # => A message from fcm about what's wrong with the request
  e.response_status # => 400
  e.details # => An array of error details from fcm
end
