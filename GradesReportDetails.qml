import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1

import "FontAwesome"

Page {
    title: "Emile - Detalhe do Boletim"

    Flickable {
        anchors { fill: parent; margins: internal.margins }
        contentWidth: parent.width
        contentHeight: detailsLayout.height

        ColumnLayout {
            id: detailsLayout
            width: parent.width-2*internal.margins
            spacing: 2*internal.margins

            Repeater {
                model: core.networkController.gradesReportDetails
                ColumnLayout {
                    spacing: internal.margins/2
                    Layout.fillWidth: true
                    Label {
                        Layout.fillWidth: true; Layout.bottomMargin: internal.margins
                        horizontalAlignment: Qt.AlignHCenter
                        text: modelData.gradesTerm
                        visible: modelData.grades.length > 0
                    }
                    Repeater {
                        Layout.fillWidth: true
                        clip: true
                        model: modelData.grades
                        Frame {
                            Layout.fillWidth: true
                            Material.elevation: 3
                            ColumnLayout {
                                width: parent.width
                                IconLabel { icon: Icons.faBook; text: modelData.acronym + " (" + modelData.type + ")" + (modelData.description !== '-' ? (" - " + modelData.description):"") }
                                IconLabel { icon: Icons.faStar; text: "Nota: " + modelData.grade + (modelData.weight !== '-' ? (" (peso " + modelData.weight + ")"):"") }
                            }
                        }
                    }
                }
            }
        }
    }
}
