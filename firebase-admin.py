import firebase_admin
from firebase_admin import credentials
from firebase_admin import exceptions
from firebase_admin import messaging

# Requires 'sudo pip install firebase-admin'

cred = credentials.Certificate(os.environ['FIREBASE_SVCACCOUNT_FILE'])
default_app = firebase_admin.initialize_app(cred)

# The topic name can be optionally prefixed with "/topics/".
topic = 'weather'

# See documentation on defining a message payload.
message = messaging.Message(
    notification=messaging.Notification(
        title='Hello from Python',
        body='Yeah!',
    ),
    topic=topic,
)

# Send a message to the devices subscribed to the provided topic.
response = messaging.send(message)

# Response is a message ID string.
print('Successfully sent message:', response)
