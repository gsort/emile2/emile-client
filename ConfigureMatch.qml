import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import "FontAwesome"

Item {
    property string nickname
    property bool isCreator: false
    property bool dirty: true

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            visible: isCreator
            text: "Informe o nome da partida aos seus amigos para que possam jogar em conjunto. Aprove a participação de cada um deles e depois clique em INICIAR PARTIDA."
        }
        Label {
            Layout.alignment: Qt.AlignHCenter
            Material.foreground: "#1b1b1b"
            Layout.topMargin: internal.margins; Layout.bottomMargin: internal.margins
            text: isCreator ? "Aguardando os outros jogadores!":"Aguardando o criador iniciar a partida"
            SequentialAnimation on scale {
                loops: Animation.Infinite
                running: true
                NumberAnimation { to: 1.1; duration: 500 }
                NumberAnimation { to: 1.0; duration: 500 }
            }
        }

        Label { text: "Jogadores desta partida"; color: "white" }
        ListView {
            id: matchesListView
            model: core.networkController.context.match.match_players
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2; clip: true
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                ItemDelegate {
                    Material.background: "red"
                    width: parent.width; Material.foreground: "#1b1b1b"
                    text: modelData.player_name + ((modelData.player_name === core.networkController.context.match.creator_name) ? " (criador da partida)":" (convidado)")
                    rightPadding: controlsLayout.width+internal.margins/2
                    RowLayout {
                        id: controlsLayout
                        spacing: 0
                        anchors { right: parent.right; verticalCenter: parent.verticalCenter }
                        RoundButton {
                            visible: modelData.approved === null && isCreator
                            Material.background: "green"; Material.foreground: "white"
                            font.family: FontAwesome.solid
                            text: Icons.faCheck
                            onClicked: core.networkController.put("matches/" + core.networkController.context.match.id + "/players/" + modelData.id + "/1", { "nickname": core.networkController.context.match.creator_name, "approved": true }, "match")
                        }
                        RoundButton {
                            visible: modelData.approved === null && isCreator
                            Material.background: "red"; Material.foreground: "white"
                            font.family: FontAwesome.solid
                            text: Icons.faTimes
                            onClicked: core.networkController.put("matches/" + core.networkController.context.match.id + "/players/" + modelData.id + "/1", { "nickname": core.networkController.context.match.creator_name, "approved": false }, "match")
                        }
                        Label {
                            visible: modelData.approved !== null
                            font.family: FontAwesome.solid
                            rightPadding: 16; leftPadding: 16
                            text: modelData.approved ? Icons.faCheck:Icons.faTimes
                            color: modelData.approved ? "green":"red"
                        }
                    }
                }
            }
        }
        Button {
            text: "iniciar partida"
            Layout.fillWidth: true
            visible: isCreator
            Material.foreground: rectangle.color
            Material.background: "#1b1b1b"
            onClicked: {
                if (core.networkController.context.match.match_players.some(player => player.approved === null)) {
                    errorDialog.text = "Você precisa aceitar ou rejeitar cada participantes antes de iniciar a partida!"
                    errorDialog.open()
                } else {
                    core.networkController.put("matches/" + core.networkController.context.match.id + "/1", { "status": 1 }, "match")
                }
            }
        }
    }
    Dialog {
        id: denyDialog
        property alias text: denyDialogText.text
        width: parent.width - 4*internal.margins
        title: "Erro"
        standardButtons: Dialog.Ok
        x: parent.width/2-width/2; y: parent.height/2-height/2
        contentItem: Label { id: denyDialogText; wrapMode: Text.WordWrap }
        onAccepted: emiletStackView.pop()
        onRejected: emiletStackView.pop()
    }
    Dialog {
        id: abortDialog
        width: parent.width - 4*internal.margins
        title: "Abortar " + (isCreator ? "partida":"participação")
        standardButtons: Dialog.Ok | Dialog.Cancel
        x: parent.width/2-width/2; y: parent.height/2-height/2
        contentItem: Label { text: "Deseja realmente abortar " + (isCreator ? "esta partida?":"sua participação nesta partida?"); wrapMode: Text.WordWrap }
        onAccepted: {
            if (isCreator) {
                emiletStackView.pop()
                core.networkController.del("matches/" + core.networkController.context.match.id);
            } else {
                var indexOf = core.networkController.context.match.match_players.findIndex(player => player.player_name === nickname)
                core.networkController.del("matches/" + core.networkController.context.match.id + "/players/" + core.networkController.context.match.match_players[indexOf].id);
            }
            emiletStackView.pop()
        }
    }
    Connections {
        target: core.networkController
        function onNewMessage(message) {
            if (denyDialog.opened) return
            if (message["action"] === "new_guest" ||
                    message["action"] === "guest_accepted" ||
                    message["action"] === "guest_rejected" ||
                    message["action"] === "guest_giveup")
                core.networkController.get("matches/" + core.networkController.context.match.id, "match")
            if (message["action"] === "guest_rejected" && message["nickname"] === nickname) {
                denyDialog.text = "O criador reprovou sua participação na partida!"
                denyDialog.open()
            }
            if (message["action"] === "match_aborted") {
                denyDialog.text = "A partida foi abortada pelo criador!"
                denyDialog.open()
            }
            if (message["action"] === "match_started") {
                emiletStackView.pop()
                emiletStackView.pop()
                emiletStackView.push("Play.qml")
            }
        }
    }
    Connections { target: backButton; function onClicked() { abortDialog.open() } }
    Component.onCompleted: core.networkController.setTopic(core.networkController.context.match.topic)
}
