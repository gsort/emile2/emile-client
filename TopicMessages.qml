import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtGraphicalEffects 1.13

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: channelDescription

    property string channelId
    property string channelDescription

    header: ColumnLayout {
        width: parent.width; spacing: 0
        ToolBar {
            id: toolbar
            Layout.fillWidth: true
            Material.foreground: "white"
            ToolButton {
                font { family: FontAwesome.solid }
                text: stackView.depth > 1 ? Icons.faChevronLeft : Icons.faBars
                onClicked: {
                    if (stackView.depth > 1) stackView.pop()
                    else drawer.open()
                }
            }
            Label { text: "Emile - Central de Mensagens"; font.bold: true; anchors.centerIn: parent }
            ToolButton {
                font { family: FontAwesome.solid }
                text: Icons.faInfo
                anchors.right: parent.right
                onClicked: infoDialog.open()
            }
        }
        ToolBar {
            Layout.fillWidth: true
            Material.foreground: "white"; Material.background: "#03728c"
            ColumnLayout {
                id: columnLayout
                anchors.centerIn: parent; spacing: 0
                Label { id: courseLabel; font.bold: true; text: stackView.currentItem.title }
                Label {
                    font.pixelSize: courseLabel.font.pixelSize*0.75;
                    text: (core.networkController.context.newMessagesCountForTopic === 0) ? "nenhuma mensagem nova":
                          core.networkController.context.newMessagesCountForTopic + ((core.networkController.context.newMessagesCountForTopic > 1) ? " mensagens novas":" mensagem nova")
                    horizontalAlignment: Text.AlignHCenter; Layout.fillWidth: true
                }
            }
        }
    }

    ListView {
        id: listView
        z: 2; clip: true; spacing: internal.margins/2
        anchors { fill: parent; margins: internal.margins }
        model: core.networkController.context.messagesForTopic
        onCountChanged: currentIndex = count-1
        delegate: RowLayout {
            width: listView.width
            spacing: internal.margins/2
            Rectangle {
                id: rectImage
                Layout.preferredHeight: toolbar.height; Layout.preferredWidth: height; Layout.alignment: Qt.AlignBottom
                radius: toolbar.height/2
                color: "#1b1b1b"
                border { width: 2; color: "white" }
                Image {
                    id: senderImage
                    anchors.centerIn: parent
                    width: rectImage.width-4; height: width
                    fillMode: Image.PreserveAspectCrop
                    source: modelData.sender_avatar
                    antialiasing: true
                    layer.enabled: !source.toString().startsWith('qrc:/')
                    layer.effect: OpacityMask { maskSource: Rectangle { width: senderImage.width; height: width; radius: width/2; visible: true } }
                }
            }
            Frame {
                Material.elevation: 3
                Layout.fillWidth: true
                ColumnLayout {
                    anchors.fill: parent
                    RowLayout {
                        Layout.fillWidth: true
                        Label { Layout.alignment: Qt.AlignLeft; Layout.fillWidth: true; font.bold: true; color: "#03728c"; text: modelData.title; wrapMode: Text.WordWrap }
                        Label {
                            Layout.alignment: Qt.AlignRight
                            font { family: FontAwesome.solid }
                            text: index >= listView.model.length-core.networkController.context.newMessagesCountForTopic ? Icons.faEnvelope:Icons.faEnvelopeOpen
                            color: index >= listView.model.length-core.networkController.context.newMessagesCountForTopic ? "green":"darkGray"
                        }
                    }
                    Label { Layout.fillWidth: true; text: modelData.body; wrapMode: Text.WordWrap; Layout.bottomMargin: parent.spacing*2 }
                    RowLayout {
                        Layout.fillWidth: true
                        Label { Layout.fillWidth: true; horizontalAlignment: Qt.AlignLeft; text: "Por: " + modelData.sender_name; font { pixelSize: courseLabel.font.pixelSize*0.75; bold: true } color: "darkGray" }
                        Label { Layout.fillWidth: true; horizontalAlignment: Qt.AlignRight; text: new Date(modelData.sent_at).toLocaleDateString("pt-BR") + " " + new Date(modelData.sent_at).toLocaleTimeString("pt-BR"); font { pixelSize: courseLabel.font.pixelSize*0.75; bold: true } color: "darkGray" }
                    }
                }
            }
        }
    }
    Image {
        anchors.fill: parent
        source: "qrc:/images/background.png"
        fillMode: Image.PreserveAspectCrop
    }

    Component.onCompleted: core.networkController.getMessagesForTopic(channelId)
}
