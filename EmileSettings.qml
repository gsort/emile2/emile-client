import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.1

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: "Emile - Configurações"

    property var settings
    property bool autoOpen: false

    ColumnLayout {
        z: 2
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Rectangle {
            id: avatarRectangle
            Layout.fillWidth: true; implicitHeight: childrenRect.height
            color: "#1b1b1b"; opacity: 0.85; radius: 4
            Label {
                padding: 10; color: "white"
                width: parent.width
                text: "Seu Avatar"
                horizontalAlignment: Qt.AlignHCenter
                font.bold: true
                wrapMode: Text.WordWrap
            }
        }
        ToolButton {
            id: avatarToolButton
            Layout.preferredWidth: parent.width/5; Layout.preferredHeight: Layout.preferredWidth; Layout.alignment: Qt.AlignHCenter
            padding: 0
            Image {
                id: currentAvatarImage
                anchors.fill: parent
                source: settings.avatar
                layer.enabled: !source.toString().startsWith('qrc:/')
                layer.effect: OpacityMask {
                    maskSource: Rectangle { width: currentAvatarImage.width; height: width; radius: width/2; visible: false }
                }
            }
            onClicked: avatarFrame.Layout.maximumHeight = (avatarFrame.Layout.maximumHeight === 0) ? controlItem.height:0
        }
        Label { id: changeLabel; text: "clique no ícone para alterar"; Layout.alignment: Qt.AlignHCenter }
        RowLayout {
            spacing: 0
            Item { id: controlItem; Layout.fillHeight: true; Layout.maximumWidth: 0 }
            Frame {
                id: avatarFrame
                Layout.fillWidth: true; Layout.fillHeight: true; Layout.alignment: Qt.AlignTop
                Layout.maximumHeight: 0
                Material.elevation: (Layout.maximumHeight === 0) ? 0:3
                Behavior on Layout.maximumHeight { NumberAnimation { duration: 500; easing.type: Easing.OutBounce } }
                padding: 0; topPadding: 0; bottomPadding: 0
                clip: true
                Flickable {
                    anchors { fill: parent; margins: internal.margins }
                    contentHeight: avatarGrid.height
                    clip: true
                    ScrollIndicator.vertical: ScrollIndicator {}
                    GridLayout {
                        id: avatarGrid
                        columns: 5
                        width: parent.width
                        Repeater {
                            id: repeater
                            model: [
                                "qrc:/images/avatar_001.png", "qrc:/images/avatar_002.png", "qrc:/images/avatar_003.png", "qrc:/images/avatar_004.png", "qrc:/images/avatar_005.png",
                                "qrc:/images/avatar_006.png", "qrc:/images/avatar_007.png", "qrc:/images/avatar_008.png", "qrc:/images/avatar_009.png", "qrc:/images/avatar_010.png",
                                "qrc:/images/avatar_011.png", "qrc:/images/avatar_012.png", "qrc:/images/avatar_013.png", "qrc:/images/avatar_014.png", "qrc:/images/avatar_015.png",
                                "qrc:/images/avatar_016.png", "qrc:/images/avatar_017.png", "qrc:/images/avatar_018.png", "qrc:/images/avatar_019.png", "qrc:/images/avatar_020.png",
                                "qrc:/images/avatar_021.png", "qrc:/images/avatar_022.png", "qrc:/images/avatar_023.png", "qrc:/images/avatar_024.png", "qrc:/images/avatar_025.png",
                                "qrc:/images/avatar_026.png", "qrc:/images/avatar_027.png", "qrc:/images/avatar_028.png", "qrc:/images/avatar_029.png", "qrc:/images/avatar_030.png",
                                "qrc:/images/avatar_031.png", "qrc:/images/avatar_032.png", "qrc:/images/avatar_033.png", "qrc:/images/avatar_034.png", "qrc:/images/avatar_035.png",
                                "qrc:/images/avatar_036.png", "qrc:/images/avatar_037.png", "qrc:/images/avatar_038.png", "qrc:/images/avatar_039.png", "qrc:/images/avatar_040.png",
                                "qrc:/images/avatar_041.png", "qrc:/images/avatar_042.png", "qrc:/images/avatar_043.png", "qrc:/images/avatar_044.png", "qrc:/images/avatar_045.png",
                                "qrc:/images/avatar_046.png", "qrc:/images/avatar_047.png", "qrc:/images/avatar_048.png", "qrc:/images/avatar_049.png", "qrc:/images/avatar_050.png",
                                "qrc:/images/avatar_051.png", "qrc:/images/avatar_052.png", "qrc:/images/avatar_053.png", "qrc:/images/avatar_054.png", "qrc:/images/avatar_055.png",
                                "qrc:/images/avatar_056.png", "qrc:/images/avatar_057.png", "qrc:/images/avatar_058.png", "qrc:/images/avatar_059.png", "qrc:/images/avatar_060.png",
                                "qrc:/images/avatar_061.png", "qrc:/images/avatar_062.png", "qrc:/images/avatar_063.png", "qrc:/images/avatar_064.png", "qrc:/images/avatar_065.png",
                                "qrc:/images/avatar_066.png", "qrc:/images/avatar_067.png", "qrc:/images/avatar_068.png", "qrc:/images/avatar_069.png", "qrc:/images/avatar_070.png",
                                "qrc:/images/avatar_071.png", "qrc:/images/avatar_072.png", "qrc:/images/avatar_073.png", "qrc:/images/avatar_074.png", "qrc:/images/avatar_075.png",
                                "qrc:/images/avatar_076.png", "qrc:/images/avatar_077.png", "qrc:/images/avatar_078.png", "qrc:/images/avatar_079.png", "qrc:/images/avatar_080.png",
                                "qrc:/images/avatar_081.png", "qrc:/images/avatar_082.png", "qrc:/images/avatar_083.png", "qrc:/images/avatar_084.png", "qrc:/images/avatar_085.png",
                                "qrc:/images/avatar_086.png", "qrc:/images/avatar_087.png", "qrc:/images/avatar_088.png", "qrc:/images/avatar_089.png", "qrc:/images/avatar_090.png",
                                "qrc:/images/avatar_091.png", "qrc:/images/avatar_092.png", "qrc:/images/avatar_093.png", "qrc:/images/avatar_094.png", "qrc:/images/avatar_095.png",
                                "qrc:/images/avatar_096.png", "qrc:/images/avatar_097.png", "qrc:/images/avatar_098.png", "qrc:/images/avatar_099.png", "qrc:/images/avatar_100.png",
                                "qrc:/images/avatar_101.png", "qrc:/images/avatar_102.png", "qrc:/images/avatar_103.png", "qrc:/images/avatar_104.png", "qrc:/images/avatar_105.png",
                                "qrc:/images/avatar_106.png", "qrc:/images/avatar_107.png", "qrc:/images/avatar_108.png", "qrc:/images/avatar_109.png", "qrc:/images/avatar_110.png"
                            ]
                            Image {
                                id: avatarImage
                                source: repeater.model[index]
                                Layout.preferredWidth: (avatarGrid.width-6*avatarGrid.columnSpacing)/5; Layout.preferredHeight: Layout.preferredWidth
                                layer.enabled: !source.toString().startsWith('qrc:/')
                                layer.effect: OpacityMask {
                                    maskSource: Rectangle { width: avatarImage.width; height: width; radius: width/2; visible: false }
                                }
                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        settings.avatar = repeater.model[index]
                                        avatarFrame.Layout.maximumHeight = 0
                                        if (autoOpen) stackView.pop()
                                    }
                                }
                            }
                            Component.onCompleted: {
                                if (settings.photoUrl !== "") {
                                    var newModel = repeater.model
                                    newModel.unshift(settings.photoUrl)
                                    repeater.model = newModel
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Image {
        anchors.fill: parent
        source: "qrc:/images/background.png"
        fillMode: Image.PreserveAspectCrop
    }

    Component.onCompleted: if (autoOpen) avatarFrame.Layout.maximumHeight = controlItem.height
}
