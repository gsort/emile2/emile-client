import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: "Emile - Central de Mensagens"

    property var settings

    Rectangle {
        z: 2
        anchors.centerIn: parent; width: parent.width-2*internal.margins; implicitHeight: childrenRect.height
        color: "#1b1b1b"; opacity: 0.85; radius: 4
        visible: core.networkController.context.currentCourses === undefined || core.networkController.context.currentCourses.length === 0
        Label {
            padding: 10; color: "white"
            width: parent.width
            text: "Você precisa fazer o login no SUAP para que os seus grupos de mensagens sejam (re)identificados!"
            horizontalAlignment: Qt.AlignHCenter
            font.bold: true
            wrapMode: Text.WordWrap
        }
    }

    ListView {
        z: 2
        anchors { fill: parent; margins: internal.margins }
        model: core.networkController.context.currentCourses
        spacing: internal.margins/2
        delegate: Item {
            width: parent.width
            height: frame.height
            Frame {
                id: frame
                width: parent.width
                Material.elevation: 3
                RowLayout {
                    width: parent.width
                    spacing: internal.margins
                    Image {
                        Layout.preferredHeight: columnLayout.height; Layout.preferredWidth: Layout.preferredHeight
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/" + ((modelData.name.startsWith('studentsOfProgram') || modelData.name.startsWith('professorsOfProgram')) ?
                                                      "emile_notification_program.png"
                                                    : (modelData.name.startsWith('general') ? "emile_notification_emile.png":"emile_notification.png"))
                        antialiasing: true
                    }
                    ColumnLayout {
                        id: columnLayout
                        Layout.fillWidth: true
                        spacing: internal.margins/2
                        Label { text: modelData.name.startsWith('studentsOfProgram') ? "Alunos do curso":(modelData.name.startsWith('professorsOfProgram')) ?
                                                                                           "Professores do curso"
                                                                                         :
                                                                                           (modelData.name.startsWith('general') ? "Todos os usuários":"Alunos da disciplina")
                            color: (modelData.name.startsWith('studentsOfProgram') || modelData.name.startsWith('professoresOfProgram')) ? "#1b1b1b":"#03728c"; font.bold: true }
                        Label { text: modelData.description; Layout.fillWidth: true; elide: Text.ElideRight }
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: stackView.push("qrc:/TopicMessages.qml", { channelId: modelData.name, channelDescription: modelData.description })
            }
        }
    }
    RoundButton {
        z: 3
        anchors { bottom: parent.bottom; right: parent.right; margins: internal.margins }
        padding: 25
        font { family: FontAwesome.solid; pointSize: 12 }
        visible: core.networkController.userType === NetworkController.UserType.Professor && core.networkController.context.currentCourses.length > 0
        text: Icons.faPlus
        Material.foreground: "white"
        Material.background: "#03728c"
        onClicked: stackView.push("qrc:/MessageDestinations.qml", { settings: settings })
    }
    Image {
        anchors.fill: parent
        source: "qrc:/images/background.png"
        fillMode: Image.PreserveAspectCrop
    }
}
