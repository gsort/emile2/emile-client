import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import "FontAwesome"

Item {
    property var match: core.networkController.context.match
    function handleMatchChanged() {
        if (match !== undefined && match.match_players.some(player => player.player_name === nicknameTextField.text))
            emiletStackView.push("ConfigureMatch.qml", { nickname: nicknameTextField.text })
    }
    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: "Informe o seu nickname e selecione a partida você deseja jogar. Use sempre o mesmo nickname para que seus pontos sejam acumulados."
        }
        GridLayout {
            columns: 2
            Layout.topMargin: internal.margins
            Label { font.family: FontAwesome.solid; text: Icons.faHourglassEnd; color: "white"; Layout.alignment: Qt.AlignHCenter }
            Label { text: " = partidas aguardando jogadores"; color: "white" }
            Label { font.family: FontAwesome.solid; text: Icons.faGamepad; color: "white"; Layout.alignment: Qt.AlignHCenter }
            Label { text: " = partidas em andamento"; color: "white" }
        }
        Label { id: nicknameLabel; text: "Seu nickname"; color: "white"; Layout.bottomMargin: -parent.spacing; Layout.topMargin: internal.margins }
        TextField {id: nicknameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: nicknameLabel.font.pixelSize; Layout.bottomMargin: internal.margins*1.5 }
        Label { text: "Selecione a partida que você deseja jogar"; color: "white" }
        ListView {
            id: matchesComboBox
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2; clip: true
            model: core.networkController.context["ongoing-matches"]
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                ItemDelegate {
                    Material.background: "red"
                    width: parent.width; Material.foreground: "#1b1b1b"
                    text: "<b>" + modelData.description + "</b> (Emilet: " + modelData.emilet.name + ")<br/>Criador: " + modelData.creator
                    rightPadding: 40
                    enabled: modelData.status === 0
                    Label {
                        anchors { right: parent.contentItem.right; rightMargin: (modelData.status === 0 ? 18:15)-parent.rightPadding; verticalCenter: parent.contentItem.verticalCenter }
                        font.family: FontAwesome.solid
                        text: (modelData.status === 0) ? Icons.faHourglassEnd:Icons.faGamepad
                    }
                    onClicked: {
                        if (nicknameTextField.text === "") {
                            errorDialog.text = "Informe seu nickname antes de prosseguir!"
                            errorDialog.open()
                        } else {
                            core.networkController.post("matches/" + core.networkController.context["ongoing-matches"][index].id + "/players/1",
                                                   { "nickname": nicknameTextField.text },
                                                   "match")
                        }
                    }
                }
            }
        }
    }
    Connections {
        target: core.networkController
        function onNewMessage(message) {
            if (message["action"] === "refresh_matches") {
                core.networkController.get("matches_by_type/0", "ongoing-matches")
            }
        }
        function onErrorStringChanged() {
            errorDialog.text = core.networkController.errorString
            errorDialog.open()
        }
    }
    StackView.onActivated: {
        onMatchChanged.connect(handleMatchChanged)
        core.networkController.get("matches_by_type/0", "ongoing-matches")
        core.networkController.setTopic("matches")
    }
    StackView.onDeactivated: {
        onMatchChanged.disconnect(handleMatchChanged)
    }
}
