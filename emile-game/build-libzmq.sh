#!/bin/bash

export ANDROID_NDK=/data/teste-livro/android-ndk-r20

rm -rf usr-*
mkdir usr-arm64-v8a usr-armeabi-v7a usr-gcc_64

# Compilando a libzmq
git clone https://github.com/zeromq/libzmq.git
cd libzmq
git checkout v4.3.2
mkdir build
cd build

cmake ../ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-gcc_64/ -DBUILD_TESTS=OFF
make -j 5
make install

rm -rf *
cmake ../ -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-arm64-v8a/ -DCMAKE_SYSROOT=$ANDROID_NDK/platforms/android-29/arch-arm64 -DCMAKE_ANDROID_API=29 -DANDROID_PLATFORM=29 -DBUILD_TESTS=OFF -DANDROID_ABI=arm64-v8a
make -j 5
make install

rm -rf *
cmake ../ -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-armeabi-v7a/ -DCMAKE_SYSROOT=$ANDROID_NDK/platforms/android-29/arch-arm -DCMAKE_ANDROID_API=29 -DANDROID_PLATFORM=29 -DBUILD_TESTS=OFF -DANDROID_ABI=armeabi-v7a
make -j 5
make install

cd ../../
rm -rf libzmq

# Compilando a cppzmq
git clone https://github.com/zeromq/cppzmq.git
cd cppzmq
git checkout v4.6.0
mkdir build
cd build

cmake ../ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-gcc_64/ -DCMAKE_PREFIX_PATH=../../usr-gcc_64/
make -j 5
make install

rm -rf *
cmake ../ -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-arm64-v8a/ -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE=BOTH -DCMAKE_PREFIX_PATH=../../usr-arm64-v8a/ -DCMAKE_SYSROOT=$ANDROID_NDK/platforms/android-29/arch-arm64 -DCMAKE_ANDROID_API=29 -DANDROID_PLATFORM=29 -DBUILD_TESTS=OFF -DANDROID_ABI=arm64-v8a
make -j 5
make install

rm -rf *
cmake ../ -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../usr-armeabi-v7a/ -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE=BOTH -DCMAKE_PREFIX_PATH=../../usr-armeabi-v7a/ -DCMAKE_SYSROOT=$ANDROID_NDK/platforms/android-29/arch-arm -DCMAKE_ANDROID_API=29 -DANDROID_PLATFORM=29 -DBUILD_TESTS=OFF -DANDROID_ABI=armeabi-v7a
make -j 5
make install

cd ../../
rm -rf cppzmq
