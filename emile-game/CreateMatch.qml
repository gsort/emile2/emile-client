import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

Item {
    property var match: networkcontroller.context.match
    function handleMatchChanged() {
        if (match !== undefined && match.match_players.some(player => player.player_name === nicknameTextField.text))
            stackView.push("ConfigureMatch.qml", { nickname: nicknameTextField.text, isCreator: true })
    }
    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: "Informe seu nickname, o nome da partida e qual Emilet você deseja jogar. Use sempre o mesmo nickname para que seus pontos sejam acumulados. Após isso, clique em RECEBER CONVIDADOS."
        }
        Flickable {
            Layout.fillWidth: true; Layout.fillHeight: true
            contentHeight: columnLayout.height
            clip: true

            ColumnLayout {
                id: columnLayout
                width: parent.width
                spacing: settings.margins
                Label { text: "Seu nickname"; color: "white"; Layout.bottomMargin: -parent.spacing }
                TextField {id: nicknameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: matchNameLabel.font.pixelSize }
                Label { id: matchNameLabel; text: "Nome da partida"; color: "white"; Layout.bottomMargin: -parent.spacing }
                TextField { id: matchNameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: matchNameLabel.font.pixelSize; wrapMode: Text.WordWrap }
                Label { text: "Selecione o Emilet que você deseja jogar"; color: "white"; Layout.bottomMargin: -parent.spacing/2 }
                ComboBox {
                    id: emiletsComboBox
                    Layout.fillWidth: true
                    model: networkcontroller.context.emilets
                    textRole: "name"
                }
                Button {
                    text: "receber convidados"
                    Layout.fillWidth: true
                    Material.foreground: rectangle.color
                    Material.background: "#1b1b1b"
                    onClicked: {
                        if (nicknameTextField.text === "" || matchNameTextField.text === "") {
                            errorDialog.text = "Informe seu nickname e nome da partida antes de prosseguir!"
                            errorDialog.open()
                        } else {
                            networkcontroller.post(
                                "emilets/" + networkcontroller.context.emilets[emiletsComboBox.currentIndex].id + "/matches",
                                { "description": matchNameTextField.text, "nickname": nicknameTextField.text }, "match", true)
                        }
                    }
                }
            }
        }
    }
    StackView.onActivated: {
        onMatchChanged.connect(handleMatchChanged)
        networkcontroller.get("emilets", "emilets")
    }
    StackView.onDeactivated: {
        onMatchChanged.disconnect(handleMatchChanged)
    }
}
