#include "zeromqsubscriberthread.h"

#include <QJsonDocument>
#include <QJsonObject>

#include <QDebug>

ZeroMQSubscriberThread::ZeroMQSubscriberThread(const QString &url, QObject *parent)
    : QThread(parent),
      _url(url),
      _context(1),
      _subscriber(_context, ZMQ_SUB)
{
}

void ZeroMQSubscriberThread::run()
{
    _subscriber.connect(_url.toStdString());

    qDebug() << "Starting subscriber thread";
    while(true)
    {
        //  Wait for next request from client
        std::string topic = s_recv(_subscriber);
        std::string string = s_recv(_subscriber);

        qDebug() << "New message:" << QJsonDocument::fromJson(QByteArray::fromStdString(string)).object();
        Q_EMIT newMessage(QJsonDocument::fromJson(QByteArray::fromStdString(string)).object());
    }
}

void ZeroMQSubscriberThread::setTopic(QString topic)
{
    if (!_topic.isEmpty())
        _subscriber.setsockopt(ZMQ_UNSUBSCRIBE, _topic.toStdString().c_str(), _topic.toStdString().size());
    _topic = topic;
    _subscriber.setsockopt(ZMQ_SUBSCRIBE, _topic.toStdString().c_str(), _topic.toStdString().size());
    qDebug() << "Subscriber topic changed to" << _topic;
}

QString ZeroMQSubscriberThread::topic() const
{
    return _topic;
}
