import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

Item {
    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            color: "white"; text: "Let's get started!"
            wrapMode: Text.WordWrap
        }
        Label {
            id: counter
            font { family: gameFont.name; pixelSize: 64 }
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            property var count: [ "1", "2", "3" ]
            property var countIndex: 0
            color: "white"
            wrapMode: Text.WordWrap
            Behavior on text {
                SequentialAnimation {
                    NumberAnimation { target: counter; property: "scale"; from: 0; to: 1; duration: 1000; easing.type: Easing.OutElastic }
                    PauseAnimation { duration: 500 }
                    onRunningChanged: {
                        if (!running && counter.countIndex < counter.count.length-1)
                            counter.text = counter.count[++counter.countIndex]
                    }
                }
            }
            Component.onCompleted: text = count[countIndex]
        }
        Item { Layout.fillHeight: true }
    }
}
