#ifndef NETWORKCONTROLLER_H
#define NETWORKCONTROLLER_H

#include <QJsonArray>
#include <QNetworkAccessManager>
#include <QObject>
#include <QQmlPropertyMap>

#include "zhelpers.h"
#include "zeromqsubscriberthread.h"

class EmileNetworkReply : public QObject
{
    Q_OBJECT
public:
    explicit EmileNetworkReply(QNetworkReply *networkReply);

Q_SIGNALS:
    void finished(const QJsonValue &jsonValue);
    void errorStringChanged(const QString &errorString);
};

class NetworkController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QQmlPropertyMap * context READ context CONSTANT)
    Q_PROPERTY(QString errorString READ errorString NOTIFY errorStringChanged)

public:
    explicit NetworkController(QObject *parent = nullptr);

    enum class Status { LOADING = 0, READY };
    Q_ENUM(Status)

    // QML API (RESTful invokable functions)
    Q_INVOKABLE void get(const QString &endpoint, const QString &resourceName = QStringLiteral(""));
    Q_INVOKABLE void post(const QString &endpoint, const QJsonObject &resourceData, const QString &resourceName = QStringLiteral(""), bool singleReturn = false);
    Q_INVOKABLE void put(const QString &endpoint, const QJsonObject &resourceData, const QString &resourceName = QStringLiteral(""), bool singleReturn = false);
    Q_INVOKABLE void del(const QString &endpoint, const QString &resourceName = QStringLiteral(""));

    // QML API (ZeroMQ invokable functions)
    Q_INVOKABLE void sendZeroMQMessage(const QString &message);
    Q_INVOKABLE void setTopic(QString topic);

    // Accessor functions
    Status status() const;
    QString errorString() const;
    QQmlPropertyMap *context();

Q_SIGNALS:
    void statusChanged();
    void errorStringChanged(const QString &errorString);
    void newMessage(const QJsonObject &message);

private:
    enum class RequestType { GET = 0, POST, PUT, DELETE };
    EmileNetworkReply *request(const QString &url, RequestType requestType = RequestType::GET, const QJsonValue &jsonValue = QJsonValue{});
    void setStatus(Status status);
    void setErrorString(const QString errorString);
    void connectToZeroMQProxy();

    //static constexpr char _RESTFULSERVERURL[] = "https://emile.sandroandrade.org";
    static constexpr char _RESTFULSERVERURL[] = "http://localhost:3000";
    static constexpr char _ZMQSERVERURL[] = "tcp://emile.sandroandrade.org";

    QNetworkAccessManager _networkAccessManager;
    QNetworkRequest _networkRequest;

    Status _status;
    QQmlPropertyMap _qmlPropertyMap;
    QString _errorString;

    zmq::context_t _context;
    zmq::socket_t _publisher;
    ZeroMQSubscriberThread _zeroMQSubscribeThread;
};

#endif // NETWORKCONTROLLER_H
