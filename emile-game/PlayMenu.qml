import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

Item {
    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Flickable {
            Layout.fillWidth: true; Layout.fillHeight: true
            contentHeight: gridLayout.height
            clip: true
            GridLayout {
                id: gridLayout
                columns: 2; columnSpacing: settings.margins/2; rowSpacing: settings.margins/2
                width: parent.width
                Repeater {
                    model: [
                        { icon: "\uf059", text: "criar uma nova partida", page: "CreateMatch.qml" },
                        { icon: "\uf058", text: "participar de uma partida", page: "JoinMatch.qml" }
                    ]
                    GameButton {
                        Layout.fillWidth: true; Layout.fillHeight: true
                        iconLabel { text: modelData.icon; color: rectangle.color }
                        text: modelData.text
                        onClicked: stackView.push(modelData.page)
                    }
                }
            }
            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }
}
