import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

Item {
    property var match: networkcontroller.context.match
    function handleMatchChanged() {
        if (match !== undefined && match.match_players.some(player => player.player_name === nicknameTextField.text))
            stackView.push("ConfigureMatch.qml", { nickname: nicknameTextField.text })
    }
    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: "Informe o seu nickname e selecione a partida você deseja jogar. Use sempre o mesmo nickname para que seus pontos sejam acumulados."
        }
        GridLayout {
            columns: 2
            Label { font.family: awesomeFont.name; text: "\uf253"; color: "white"; Layout.alignment: Qt.AlignHCenter }
            Label { text: " = partidas aguardando jogadores"; color: "white" }
            Label { font.family: awesomeFont.name; text: "\uf11b"; color: "white"; Layout.alignment: Qt.AlignHCenter }
            Label { text: " = partidas em andamento"; color: "white" }
        }
        Label { id: nicknameLabel; text: "Seu nickname"; color: "white"; Layout.bottomMargin: -parent.spacing }
        TextField {id: nicknameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: nicknameLabel.font.pixelSize }
        Label { text: "Selecione a partida que você deseja jogar"; color: "white"; Layout.bottomMargin: -parent.spacing/2 }
        ListView {
            id: matchesComboBox
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: settings.margins/4; clip: true
            model: networkcontroller.context["ongoing-matches"]
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                ItemDelegate {
                    Material.background: "red"
                    width: parent.width; Material.foreground: "#1b1b1b"
                    text: "<b>" + modelData.description + "</b> (Emilet: " + modelData.emilet.name + ")<br/>Criador: " + modelData.creator
                    rightPadding: 40
                    enabled: modelData.status === 0
                    Label {
                        anchors { right: parent.contentItem.right; rightMargin: (modelData.status === 0 ? 18:15)-parent.rightPadding; verticalCenter: parent.contentItem.verticalCenter }
                        font.family: awesomeFont.name
                        text: (modelData.status === 0) ? "\uf253":"\uf11b"
                    }
                    onClicked: {
                        if (nicknameTextField.text === "") {
                            errorDialog.text = "Informe seu nickname antes de prosseguir!"
                            errorDialog.open()
                        } else {
                            networkcontroller.post("matches/" + networkcontroller.context["ongoing-matches"][index].id + "/players",
                                                   { "nickname": nicknameTextField.text },
                                                   "match", true)
                        }
                    }
                }
            }
        }
    }
    Connections {
        target: networkcontroller
        onNewMessage: {
            if (message["action"] === "refresh_matches") {
                networkcontroller.get("matches_by_type/0", "ongoing-matches")
            }
        }
        onErrorStringChanged: {
            errorDialog.text = networkcontroller.errorString
            errorDialog.open()
        }
    }
    StackView.onActivated: {
        onMatchChanged.connect(handleMatchChanged)
        networkcontroller.get("matches_by_type/0", "ongoing-matches")
        networkcontroller.setTopic("matches")
    }
    StackView.onDeactivated: {
        onMatchChanged.disconnect(handleMatchChanged)
    }
}
