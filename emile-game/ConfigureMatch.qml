import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

Item {
    property string nickname
    property bool isCreator: false
    property bool dirty: true

    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            visible: isCreator
            text: "Informe o nome da partida aos seus amigos para que possam jogar em conjunto. Aprove a participação de cada um deles e depois clique em INICIAR PARTIDA."
        }
        Label {
            Layout.alignment: Qt.AlignHCenter
            Material.foreground: "#1b1b1b"
            text: isCreator ? "Aguardando os outros jogadores!":"Aguardando o criador iniciar a partida"
            SequentialAnimation on scale {
                loops: Animation.Infinite
                running: true
                NumberAnimation { to: 1.1; duration: 500 }
                NumberAnimation { to: 1.0; duration: 500 }
            }
        }

        Label { text: "Jogadores desta partida"; color: "white"; Layout.bottomMargin: -parent.spacing/2 }
        ListView {
            id: matchesListView
            model: networkcontroller.context.match.match_players
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: settings.margins/4; clip: true
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                ItemDelegate {
                    Material.background: "red"
                    width: parent.width; Material.foreground: "#1b1b1b"
                    text: modelData.player_name + ((modelData.player_name === networkcontroller.context.match.creator) ? " (criador da partida)":" (convidado)")
                    rightPadding: controlsLayout.width
                    RowLayout {
                        id: controlsLayout
                        spacing: 0
                        anchors { right: parent.right; verticalCenter: parent.verticalCenter }
                        RoundButton {
                            visible: modelData.approved === null && isCreator
                            Material.background: "green"; Material.foreground: "white"
                            font.family: awesomeFont.name
                            text: "\uf00c"
                            onClicked: networkcontroller.put("matches/" + networkcontroller.context.match.id + "/players/" + modelData.id, { "nickname": networkcontroller.context.match.creator, "approved": true }, "match", true)
                        }
                        RoundButton {
                            visible: modelData.approved === null && isCreator
                            Material.background: "red"; Material.foreground: "white"
                            font.family: awesomeFont.name
                            text: "\uf00d"
                            onClicked: networkcontroller.put("matches/" + networkcontroller.context.match.id + "/players/" + modelData.id, { "nickname": networkcontroller.context.match.creator, "approved": false }, "match", true)
                        }
                        Label {
                            visible: modelData.approved !== null
                            font.family: awesomeFont.name
                            rightPadding: 16; leftPadding: 16
                            text: modelData.approved ? "\uf00c":"\uf00d"
                            color: modelData.approved ? "green":"red"
                        }
                    }
                }
            }
        }
        Button {
            text: "iniciar partida"
            Layout.fillWidth: true
            visible: isCreator
            Material.foreground: rectangle.color
            Material.background: "#1b1b1b"
            onClicked: {
                if (networkcontroller.context.match.match_players.some(player => player.approved === null)) {
                    errorDialog.text = "Você precisa aceitar ou rejeitar cada participantes antes de iniciar a partida!"
                    errorDialog.open()
                } else {
                    networkcontroller.put("matches/" + networkcontroller.context.match.id, { "status": 1 }, "match", true)
                }
            }
        }
    }
    Dialog {
        id: denyDialog
        property alias text: denyDialogText.text
        width: parent.width - 2*settings.margins
        title: "Erro"
        standardButtons: Dialog.Ok
        anchors.centerIn: parent
        contentItem: Label { id: denyDialogText; wrapMode: Text.WordWrap }
        onAccepted: stackView.pop()
        onRejected: stackView.pop()
    }
    Dialog {
        id: abortDialog
        width: parent.width - 2*settings.margins
        title: "Abortar " + (isCreator ? "partida":"participação")
        standardButtons: Dialog.Ok | Dialog.Cancel
        anchors.centerIn: parent
        contentItem: Label { text: "Deseja realmente abortar " + (isCreator ? "esta partida?":"sua participação nesta partida?"); wrapMode: Text.WordWrap }
        onAccepted: {
            if (isCreator) {
                stackView.pop()
                networkcontroller.del("matches/" + networkcontroller.context.match.id);
            } else {
                var indexOf = networkcontroller.context.match.match_players.findIndex(player => player.player_name === nickname)
                networkcontroller.del("matches/" + networkcontroller.context.match.id + "/players/" + networkcontroller.context.match.match_players[indexOf].id);
            }
            stackView.pop()
        }
    }
    Connections {
        target: networkcontroller
        onNewMessage: {
            if (denyDialog.opened) return
            if (message["action"] === "new_guest" ||
                    message["action"] === "guest_accepted" ||
                    message["action"] === "guest_rejected" ||
                    message["action"] === "guest_giveup")
                networkcontroller.get("matches/" + networkcontroller.context.match.id, "match")
            if (message["action"] === "guest_rejected" && message["nickname"] === nickname) {
                denyDialog.text = "O criador reprovou sua participação na partida!"
                denyDialog.open()
            }
            if (message["action"] === "match_aborted") {
                denyDialog.text = "A partida foi abortada pelo criador!"
                denyDialog.open()
            }
            if (message["action"] === "match_started") {
                stackView.pop()
                stackView.pop()
                stackView.push("Play.qml")
            }
        }
    }
    Connections { target: backButton; onClicked: abortDialog.open() }
    Component.onCompleted: networkcontroller.setTopic(networkcontroller.context.match.topic)
}
