#include "networkcontroller.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

#include <qqml.h>

EmileNetworkReply::EmileNetworkReply(QNetworkReply *networkReply)
{
    connect(networkReply, &::QNetworkReply::finished, this, [=] () {
        QJsonDocument jsonDocument = QJsonDocument::fromJson(networkReply->readAll());
        if (jsonDocument.isObject()) {
            if (jsonDocument.object().value("error").isString())
                Q_EMIT errorStringChanged(jsonDocument.object().value("error").toString());
            else
                Q_EMIT finished(QJsonValue(jsonDocument.object()));
        }
        else if (jsonDocument.isArray())
            Q_EMIT finished(QJsonValue(jsonDocument.array()));
        else
            Q_EMIT finished(QJsonValue{});
        networkReply->deleteLater();
    });
}

NetworkController::NetworkController(QObject *parent)
    : QObject(parent),
      _status(Status::READY),
      _context(1),
      _publisher(_context, ZMQ_PUB),
      _zeroMQSubscribeThread(QString("%1:5560").arg(_ZMQSERVERURL))
{
    _networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    connect(&_zeroMQSubscribeThread, &ZeroMQSubscriberThread::newMessage, this, &NetworkController::newMessage);
    qmlRegisterUncreatableType<NetworkController>("br.edu.ifba.emilet", 1, 0, "NetworkController", "You cannot create an instance of NetworkController.");
    connectToZeroMQProxy();
}

void NetworkController::get(const QString &endpoint, const QString &resourceName)
{
    auto reply = request(QString("%1/%2").arg(_RESTFULSERVERURL).arg(endpoint));
    connect(reply, &EmileNetworkReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _qmlPropertyMap.insert(resourceName, jsonValue);
    });
}

void NetworkController::post(const QString &endpoint, const QJsonObject &resourceData, const QString &resourceName, bool singleReturn)
{
    auto reply = request(QString("%1/%2/%3").arg(_RESTFULSERVERURL).arg(endpoint).arg(singleReturn ? "1":"0"), RequestType::POST, resourceData);
    connect(reply, &EmileNetworkReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _qmlPropertyMap.insert(resourceName, jsonValue);
    });
}

void NetworkController::put(const QString &endpoint, const QJsonObject &resourceData, const QString &resourceName, bool singleReturn)
{
    auto reply = request(QString("%1/%2/%3").arg(_RESTFULSERVERURL).arg(endpoint).arg(singleReturn ? "1":"0"), RequestType::PUT, resourceData);
    connect(reply, &EmileNetworkReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _qmlPropertyMap.insert(resourceName, jsonValue);
    });
}

void NetworkController::del(const QString &endpoint, const QString &resourceName)
{
    auto reply = request(QString("%1/%2").arg(_RESTFULSERVERURL).arg(endpoint), RequestType::DELETE);
    connect(reply, &EmileNetworkReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _qmlPropertyMap.insert(resourceName, jsonValue);
    });
}

void NetworkController::sendZeroMQMessage(const QString &message)
{
    s_sendmore(_publisher, _zeroMQSubscribeThread.topic().toStdString());
    s_send(_publisher, message.toStdString());
}

void NetworkController::setTopic(QString topic)
{
    if (_zeroMQSubscribeThread.topic() != topic)
        _zeroMQSubscribeThread.setTopic(topic);
}

NetworkController::Status NetworkController::status() const
{
    return _status;
}

QString NetworkController::errorString() const
{
    return _errorString;
}

QQmlPropertyMap *NetworkController::context()
{
    return &_qmlPropertyMap;
}

EmileNetworkReply *NetworkController::request(const QString &url, RequestType requestType, const QJsonValue &jsonValue)
{
    _errorString.clear();
    _networkRequest.setUrl(QUrl(url));
    QNetworkReply *networkReply = nullptr;
    setStatus(Status::LOADING);
    if (requestType == RequestType::GET) {
        networkReply = _networkAccessManager.get(_networkRequest);
    } else if (requestType == RequestType::DELETE) {
        networkReply = _networkAccessManager.deleteResource(_networkRequest);
    } else {
        if (!jsonValue.isObject()) {
            qDebug() << "Error in request: jsonValue must be an object!";
            return nullptr;
        }
        if (requestType == RequestType::POST) {
            networkReply = _networkAccessManager.post(_networkRequest, QJsonDocument(jsonValue.toObject()).toJson());
        } else if (requestType == RequestType::PUT) {
            networkReply = _networkAccessManager.put(_networkRequest, QJsonDocument(jsonValue.toObject()).toJson());
        }
    }
    auto emileNetworkReply = new EmileNetworkReply(networkReply);
    connect(emileNetworkReply, &EmileNetworkReply::finished, this, [=](){ setStatus(Status::READY); });
    connect(emileNetworkReply, &EmileNetworkReply::errorStringChanged, this, [=](const QString &errorString){
        setErrorString(errorString);
        setStatus(Status::READY);
    });
    return emileNetworkReply;
}

void NetworkController::setStatus(NetworkController::Status status)
{
    if (_status != status) {
        _status = status;
        Q_EMIT statusChanged();
    }
}

void NetworkController::setErrorString(const QString errorString)
{
    if (_errorString != errorString) {
        _errorString = errorString;
        Q_EMIT errorStringChanged(_errorString);
    }
}

void NetworkController::connectToZeroMQProxy()
{
    qDebug() << "Connecting publisher to" << QString("%1:5559").arg(_ZMQSERVERURL);
    _publisher.connect(QString("%1:5559").arg(_ZMQSERVERURL).toStdString());
    _zeroMQSubscribeThread.start();
}
