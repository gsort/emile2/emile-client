import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13

Item {
    property var configurator
    property var currentSwipe: undefined
    property int currentId: -1

    ColumnLayout {
        anchors { fill: parent; margins: settings.margins }
        spacing: settings.margins

        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: configurator.description
        }
        Loader { sourceComponent: configurator.extraHeaderComponent }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: settings.margins/4; clip: true
            model: networkcontroller.context[configurator.model]
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                SwipeDelegate {
                    anchors.fill: parent; text: modelData.name; Material.foreground: "#1b1b1b"
                    swipe.left: Label {
                        id: deleteLabel
                        text: "Remover"; color: "white"
                        verticalAlignment: Label.AlignVCenter
                        padding: 12
                        height: parent.height
                        anchors.left: parent.left
                        MouseArea { anchors.fill: parent; onClicked: { currentSwipe = swipe; currentId = modelData.id; removeDialog.open() } }
                        background: Rectangle { color: deleteLabel.SwipeDelegate.pressed ? Qt.darker("tomato", 1.1) : "tomato" }
                    }
                    Label {
                        visible: configurator.approvedProperty !== undefined
                        anchors { right: parent.contentItem.right; rightMargin: 15-parent.rightPadding; verticalCenter: parent.contentItem.verticalCenter }
                        font.family: awesomeFont.name
                        text: modelData[configurator.approvedProperty] ? "\uf00c":"\uf4fc"
                    }
                    rightPadding: configurator.approvedProperty !== undefined ? 40:undefined
                    onClicked: configurator.onDetailClicked(index)
                }
            }
            populate: Transition { NumberAnimation { properties: "x, y"; duration: 2000; easing.type: Easing.OutElastic } }
        }
    }
    Dialog {
        id: removeDialog
        width: parent.width - 2*settings.margins
        title: "Remoção de item"
        standardButtons: Dialog.Ok | Dialog.Cancel
        anchors.centerIn: parent
        contentItem: Label { text: "Tem certeza que deseja remover este item?" }
        onAccepted: configurator.onDeleteClicked(currentId)
        onRejected: currentSwipe.close()
    }
    Dialog {
        id: addDialog
        width: parent.width - 2*settings.margins
        title: (configurator.addDialogTitle !== undefined) ? configurator.addDialogTitle:""
        standardButtons: Dialog.Ok | Dialog.Cancel
        anchors.centerIn: parent
        contentItem: TextField { id: textField; placeholderText: (configurator.addDialogPlaceholder !== undefined) ? configurator.addDialogPlaceholder:"" }
        onAccepted: configurator.onAddClicked(textField.text)
        onVisibleChanged: { textField.clear(); textField.forceActiveFocus() }
    }
    RoundButton {
        visible: configurator.onAddClicked !== undefined
        anchors { bottom: parent.bottom; right: parent.right; margins: 15 }
        font { family: awesomeFont.name; pointSize: 16 }
        padding: 20
        text: "\uf067"
        Material.foreground: rectangle.color; Material.background: "#1b1b1b"
        onClicked: if (configurator.useAddDialog) addDialog.open(); else configurator.onAddClicked()
    }
    Component.onCompleted: if (configurator.onCompleted !== undefined) configurator.onCompleted()
}
