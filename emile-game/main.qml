import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.edu.ifba.emilet 1.0

ApplicationWindow {
    visible: true
    width: 360; height: 600
    title: qsTr("Hello World")

    FontLoader { id: gameFont; source: "PocketMonk-15ze.ttf" }
    FontLoader { id: awesomeFont; source: "FontAwesome-5-Free-Solid-900.otf" }

    Audio { source: "Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    header: ToolBar {
        Material.foreground: "white"
        ToolButton {
            id: backButton
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            font.family: awesomeFont.name
            text: stackView.depth > 1 ? "\uf053" : ""
            onClicked: { if (stackView.depth > 1 && !stackView.currentItem.dirty) stackView.pop() }
        }
        Label { text: "Emile - Emilet!"; font.bold: true; anchors.centerIn: parent }
        ToolButton {
            id: saveButton
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            font.family: awesomeFont.name
            visible: false
            text: "\uf0c7"
        }
    }

    QtObject {
        id: settings
        property int margins: 20
    }

    BusyIndicator {
        anchors.centerIn: parent
        z: 2
        running: networkcontroller.status === NetworkController.LOADING
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 }
            ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 }
            ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 }
            ColorAnimation { to: "#8bc740"; duration: 5000 }
        }

        StackView {
            id: stackView
            anchors.fill: parent
            visible: networkcontroller.status === NetworkController.READY
            initialItem: Item {
                ColumnLayout {
                    anchors { fill: parent; margins: settings.margins }
                    spacing: settings.margins
                    Label {
                        font { family: gameFont.name; pixelSize: 64 }
                        Layout.alignment: Qt.AlignHCenter
                        color: "white"; text: "Emilet!"
                        SequentialAnimation on scale {
                            loops: Animation.Infinite
                            PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
                            PauseAnimation { duration: 10000 }
                        }
                        SequentialAnimation on rotation {
                            loops: Animation.Infinite
                            PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
                            PauseAnimation { duration: 10000 }
                        }
                    }
                    Flickable {
                        Layout.fillWidth: true; Layout.fillHeight: true
                        contentHeight: gridLayout.height
                        clip: true
                        GridLayout {
                            id: gridLayout
                            columns: 2; columnSpacing: settings.margins/2; rowSpacing: settings.margins/2
                            width: parent.width
                            Component { id: extraHeader; GridLayout {
                                    columns: 2
                                    Label { font.family: awesomeFont.name; text: "\uf4fc"; color: "white" }
                                    Label { text: " = questão aguardando por aprovação"; color: "white" }
                                    Label { font.family: awesomeFont.name; text: "\uf00c"; color: "white" }
                                    Label { text: " = questão aprovada"; color: "white" }
                                }
                            }
                            Repeater {
                                id: repeater
                                property var repeaterModel: [
                                    {
                                        icon: "\uf059",
                                        text: "enviar pergunta",
                                        description: "Uma categoria agrupa questões sobre um mesmo tópico. Selecione a categoria à qual deseja adicionar a pergunta ou crie uma nova categoria.",
                                        model: "categories",
                                        onCompleted: () => { networkcontroller.get("categories", "categories") },
                                        onDetailClicked: (index) => { stackView.push("Editor.qml", { configurator:
                                             {
                                                 description: "Selecione a questão a ser modificada ou crie uma nova questão.",
                                                 model: "questions",
                                                 onCompleted: () => { networkcontroller.get("categories/" + networkcontroller.context.categories[index].id + "/questions", "questions") },
                                                 onDetailClicked: (index) => { stackView.push("Question.qml", { update: true, question: networkcontroller.context.questions[index] }) },
                                                 onDeleteClicked: (id) => { networkcontroller.del("questions/" + id, "questions") },
                                                 onAddClicked: () => { stackView.push("Question.qml", { update: false, question: { name: "", description: "", category_id: networkcontroller.context.categories[index].id, question_options: [], right_option: -1, approved: false }}) },
                                                 approvedProperty: "approved",
                                                 extraHeaderComponent: extraHeader
                                             } }) },
                                        onDeleteClicked: (id) => { networkcontroller.del("categories/" + id, "categories") },
                                        onAddClicked: (description) => { networkcontroller.post("categories", { name: description }, "categories") },
                                        useAddDialog: true,
                                        addDialogTitle: "Nova Categoria",
                                        addDialogPlaceholder: "Digite a nova categoria",
                                    },
                                    {
                                        icon: "\uf058",
                                        text: "aprovar perguntas",
                                        description: "Clique em cada questão abaixo para fazer a revisão e aprovar a submissão.",
                                        model: "pending_questions",
                                        onCompleted: () => { networkcontroller.get("pending_questions", "pending_questions") },
                                        onDetailClicked: (index) => { stackView.push("Question.qml", { update: true, approve: true, resourceName: "pending_questions", question: networkcontroller.context.pending_questions[index] }) },
                                    },
                                    {
                                        icon: "\uf6cf",
                                        text: "configurar emilets",
                                        description: "Um Emilet agrupa categorias de questão relacionadas e define o universo de questões que serão aleatoriamente escolhidas ao executar um determinado Emilet. Selecione o Emilet que você deseja configurar ou crie um novo Emilet.",
                                        model: "emilets",
                                        onCompleted: () => { networkcontroller.get("emilets", "emilets"); networkcontroller.get("categories", "categories") },
                                        onDetailClicked: (index) => { stackView.push("Emilet.qml", { update: true, emilet: networkcontroller.context.emilets[index], emiletIndex: index }) },
                                        onDeleteClicked: (id, index) => { networkcontroller.del("emilets/" + id, "emilets") },
                                        onAddClicked: () => { stackView.push("Emilet.qml", { update: false, emilet: { name: "", categories: [] }}) },
                                    }
                                ]
                                model: repeaterModel
                                GameButton {
                                    Layout.fillWidth: true; Layout.fillHeight: true
                                    iconLabel { text: modelData.icon; color: rectangle.color }
                                    text: modelData.text
                                    onClicked: stackView.push("Editor.qml", { configurator: repeater.repeaterModel[index] })
                                }
                            }
                            GameButton {
                                Layout.fillWidth: true; Layout.fillHeight: true
                                iconLabel { text: "\uf11b"; color: rectangle.color }
                                text: "jogar"
                                onClicked: stackView.push("PlayMenu.qml")
                            }
                        }
                        ScrollIndicator.vertical: ScrollIndicator { }
                    }
                }
            }
        }
    }
    Dialog {
        id: errorDialog
        property alias text: errorDialogText.text
        width: parent.width - 2*settings.margins
        title: "Erro"
        standardButtons: Dialog.Ok
        anchors.centerIn: parent
        contentItem: Label { id: errorDialogText; wrapMode: Text.WordWrap }
    }
    onClosing: {
        if (Qt.platform.os == "android") {
            close.accepted = false
            if (stackView.depth > 1) stackView.pop()
        }
    }
    //Component.onCompleted: stackView.push("ZeroMQTest.qml")
}
