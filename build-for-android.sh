#!/bin/bash

export FIREBASE_CPP_SDK_DIR=/data/teste-livro/firebase_cpp_sdk/

#cmake -C ../qtcsettings.cmake ../
cmake '-GCodeBlocks - Ninja' -C ../qtcsettings-android.cmake ../

#make -j 2
cmake --build . --target all -j 4

#make aab
/data/teste-livro/Qt/5.15.0/android/bin/androiddeployqt --input ./android_deployment_settings.json --output ./android-build/ --android-platform android-29 --jdk /usr/lib/jvm/default --gradle --aab

echo "Signing apk"
/data/teste-livro/android-sdk/build-tools/29.0.3/apksigner sign --min-sdk-version 21  \
    --ks ../keystore.jks ./android-build/build/outputs/apk/debug/android-build-debug.apk

echo "Signing aab"
/data/teste-livro/android-sdk/build-tools/29.0.3/apksigner sign --min-sdk-version 21  \
    --ks ../keystore.jks ./android-build/build/outputs/bundle/release/android-build-release.aab
