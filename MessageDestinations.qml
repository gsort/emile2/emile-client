import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: "Emile - Nova Mensagem"

    property var settings
    property var groups: core.networkController.context.currentCourses

    ColumnLayout {
        z: 2
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins/2
        Rectangle {
            Layout.fillWidth: true; implicitHeight: childrenRect.height
            color: "#1b1b1b"; opacity: 0.85; radius: 4
            Label {
                padding: internal.margins; color: "white"
                width: parent.width
                text: "Selecione abaixo os grupos para os quais você deseja enviar a sua mensagem"
                horizontalAlignment: Qt.AlignHCenter
                font.bold: true
                wrapMode: Text.WordWrap
            }
        }
        ListView {
            id: listView
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: core.networkController.context.currentCourses
            clip: true
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3
                CheckDelegate {
                    id: checkDelegate
                    width: parent.width
                    text: " "
                    onCheckedChanged: groups[index].checked = checked
                    contentItem: ColumnLayout {
                        implicitWidth: checkDelegate.width
                        anchors { left: checkDelegate.left; right: checkDelegate.right; leftMargin: checkDelegate.horizontalPadding; rightMargin: checkDelegate.horizontalPadding + checkDelegate.indicator.width + checkDelegate.spacing }
                        spacing: internal.margins/2
                        Label { text: modelData.name.startsWith('studentsOfProgram') ? "Alunos do curso":(modelData.name.startsWith('professorsOfProgram')) ? "Professores do curso":"Alunos da disciplina"; color: (modelData.name.startsWith('studentsOfProgram') || modelData.name.startsWith('professoresOfProgram')) ? "#1b1b1b":"#03728c"; font.bold: true }
                        Label { text: modelData.description; Layout.fillWidth: true; elide: Text.ElideRight }
                    }
                }
            }
        }
        Button {
            Layout.fillWidth: true
            Material.foreground: "white"
            Material.background: "#03728c"
            text: "próximo"
            onClicked: {
                var selected = false
                var destinations = []
                for (var i = 0; i < groups.length; ++i) {
                    if (groups[i].checked) {
                        selected = true;
                        destinations.push(groups[i])
                    }
                }
                if (!selected) errorDialog.open()
                else stackView.push("qrc:/NewMessage.qml", { destinations: destinations, settings: settings })
            }
        }
    }
    Dialog {
        id: errorDialog
        title: "Erro"
        x: parent.width/2-width/2
        y: parent.height/2-height/2
        width: parent.width - 40
        Label { text: "Você deve selecionar pelo menos um grupo!" }
        standardButtons: Dialog.Ok
    }
    Image {
        anchors.fill: parent
        source: "qrc:/images/background.png"
        fillMode: Image.PreserveAspectCrop
    }
}
