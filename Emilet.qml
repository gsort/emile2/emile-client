import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13

Item {
    property var emilet
    property int emiletIndex
    property bool update
    property bool dirty: false

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins

        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label { id: nameLabel; text: "Nome"; color: "white"; Layout.bottomMargin: -parent.spacing }
        TextField {id: nameTextField; text: emilet.name; Layout.fillWidth: true; Layout.bottomMargin: parent.spacing*1.5; Material.foreground: "white"; font.pixelSize: nameLabel.font.pixelSize; onDisplayTextChanged: if (emilet.name !== nameTextField.displayText) { dirty = true; emilet.name = nameTextField.displayText } }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            text: "Categorias"
        }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2; clip: true
            model: core.networkController.context.categories
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                CheckDelegate {
                    width: parent.width; text: modelData.name; Material.foreground: "#1b1b1b"
                    checked: emilet.categories.some(category => category.id === modelData.id)
                    onClicked: {
                        dirty = true
                        if (checked)
                            emilet.categories.push({ "id": modelData.id })
                        else
                            emilet.categories.splice(emilet.categories.findIndex(category => category.id === modelData.id), 1)
                    }
                }
            }
            populate: Transition { NumberAnimation { properties: "x, y"; duration: 2000; easing.type: Easing.OutElastic } }
        }
    }
    Dialog {
        id: dirtyDialog
        width: parent.width - 20
        title: "Emilet Modificado"
        standardButtons: Dialog.Yes | Dialog.No | Dialog.Discard
        anchors.centerIn: parent
        contentItem: Label { text: "Você realizou modificações neste emilet. Deseja salvá-lo?"; wrapMode: Text.WordWrap }
        onAccepted: save()
        onRejected: emiletStackView.pop()
        onDiscarded: close()
    }
    function save() {
        if (update)
            core.networkController.put("emilets/" + emilet.id + "/0", emilet, "emilets")
        else
            core.networkController.post("emilets/0", emilet, "emilets")
        emiletStackView.pop()
    }
    Connections {
        target: saveButton
        function onClicked() { save() }
    }
    Connections {
        target: backButton
        function onClicked() { if (dirty) dirtyDialog.open() }
    }
    StackView.onActivated: saveButton.visible = Qt.binding(function() { return dirty })
    StackView.onRemoved: { saveButton.visible = false }
}
