import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1
import QtMultimedia 5.13
import QtGraphicalEffects 1.1

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: "Emilet"

    property var settings

    FontLoader { id: gameFont; source: "PocketMonk-15ze.ttf" }
    Audio { source: "Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    header: ToolBar {
        Material.foreground: "white"
        ToolButton {
            id: backButton
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            font.family: FontAwesome.solid
            text: Icons.faChevronLeft
            onClicked: {
                if (emiletStackView.depth > 1) {
                    if (!emiletStackView.currentItem.dirty) emiletStackView.pop()
                } else {
                    stackView.pop()
                }
            }
        }
        Label { text: "Emile - Emilet!"; font.bold: true; anchors.centerIn: parent }
        ToolButton {
            id: saveButton
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            font.family: FontAwesome.solid
            visible: false
            text: Icons.faSave
        }
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 }
            ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 }
            ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 }
            ColorAnimation { to: "#8bc740"; duration: 5000 }
        }

        StackView {
            id: emiletStackView
            anchors.fill: parent
            visible: core.networkController.status !== NetworkController.Loading
            initialItem: Item {
                ColumnLayout {
                    anchors { fill: parent; margins: internal.margins }
                    spacing: internal.margins
                    Label {
                        font { family: gameFont.name; pixelSize: 64 }
                        Layout.alignment: Qt.AlignHCenter
                        color: "white"; text: "Emilet!"
                        SequentialAnimation on scale {
                            loops: Animation.Infinite
                            PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
                            PauseAnimation { duration: 10000 }
                        }
                        SequentialAnimation on rotation {
                            loops: Animation.Infinite
                            PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
                            PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
                            PauseAnimation { duration: 10000 }
                        }
                    }
                    Image {
                        Layout.alignment: Qt.AlignHCenter
                        source: settings.photoUrl
                        Layout.preferredWidth: 64; Layout.preferredHeight: 64; fillMode: Image.PreserveAspectCrop
                        layer.enabled: true
                        layer.effect: OpacityMask {
                            maskSource: Rectangle { width: 64; height: width; radius: width/2; visible: false }
                        }
                    }
                    Label { Layout.alignment: Qt.AlignHCenter; text: "Olá " + settings.displayName; color: "white"; Layout.bottomMargin: settings.margins }
                    Flickable {
                        Layout.fillWidth: true; Layout.fillHeight: true
                        contentHeight: gridLayout.height
                        clip: true
                        GridLayout {
                            id: gridLayout
                            columns: 2; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
                            width: parent.width
                            Component { id: extraHeader; GridLayout {
                                    columns: 2
                                    Label { font.family: FontAwesome.solid; text: Icons.faUserCheck; color: "white" }
                                    Label { text: " = questão aguardando por aprovação"; color: "white" }
                                    Label { font.family: FontAwesome.solid; text: Icons.faCheck; color: "white" }
                                    Label { text: " = questão aprovada"; color: "white" }
                                }
                            }
                            Repeater {
                                id: repeater
                                property var repeaterModel: [
                                    {
                                        icon: Icons.faQuestionCircle,
                                        text: "enviar pergunta",
                                        description: "Uma categoria agrupa questões sobre um mesmo tópico. Selecione a categoria à qual deseja adicionar a pergunta ou crie uma nova categoria.",
                                        model: "categories",
                                        onCompleted: () => { core.networkController.get("categories", "categories") },
                                        onDetailClicked: (index) => { emiletStackView.push("Editor.qml", { configurator:
                                                                                         {
                                                                                             description: "Selecione a questão a ser modificada ou crie uma nova questão.",
                                                                                             model: "questions",
                                                                                             onCompleted: () => { core.networkController.get("categories/" + core.networkController.context.categories[index].id + "/questions", "questions") },
                                                                                             onDetailClicked: (index) => { emiletStackView.push("Question.qml", { update: true, question: core.networkController.context.questions[index] }) },
                                                                                             onDeleteClicked: (id) => { core.networkController.del("questions/" + id, "questions") },
                                                                                             onAddClicked: () => { emiletStackView.push("Question.qml", { update: false, question: { name: "", description: "", category_id: core.networkController.context.categories[index].id, question_options: [], right_option: -1, approved: false }}) },
                                                                                             approvedProperty: "approved",
                                                                                             extraHeaderComponent: extraHeader
                                                                                         } }) },
                                        onDeleteClicked: (id) => { core.networkController.del("categories/" + id, "categories") },
                                        onAddClicked: (description) => { core.networkController.post("categories/0", { name: description }, "categories") },
                                        useAddDialog: true,
                                        addDialogTitle: "Nova Categoria",
                                        addDialogPlaceholder: "Digite a nova categoria",
                                    },
                                    {
                                        icon: Icons.faCheckCircle,
                                        text: "aprovar perguntas",
                                        description: "Clique em cada questão abaixo para fazer a revisão e aprovar a submissão.",
                                        model: "pending_questions",
                                        onCompleted: () => { core.networkController.get("pending_questions", "pending_questions") },
                                        onDetailClicked: (index) => { emiletStackView.push("Question.qml", { update: true, approve: true, resourceName: "pending_questions", question: core.networkController.context.pending_questions[index] }) },
                                    },
                                    {
                                        icon: Icons.faDiceD20,
                                        text: "configurar emilets",
                                        description: "Um Emilet agrupa categorias de questão relacionadas e define o universo de questões que serão aleatoriamente escolhidas ao executar um determinado Emilet. Selecione o Emilet que você deseja configurar ou crie um novo Emilet.",
                                        model: "emilets",
                                        onCompleted: () => { core.networkController.get("emilets", "emilets"); core.networkController.get("categories", "categories") },
                                        onDetailClicked: (index) => { emiletStackView.push("Emilet.qml", { update: true, emilet: core.networkController.context.emilets[index], emiletIndex: index }) },
                                        onDeleteClicked: (id, index) => { core.networkController.del("emilets/" + id, "emilets") },
                                        onAddClicked: () => { emiletStackView.push("Emilet.qml", { update: false, emilet: { name: "", categories: [] }}) },
                                    }
                                ]
                                model: repeaterModel
                                GameButton {
                                    Layout.fillWidth: true; Layout.fillHeight: true
                                    iconLabel { text: modelData.icon; color: rectangle.color }
                                    text: modelData.text
                                    onClicked: emiletStackView.push("Editor.qml", { configurator: repeater.repeaterModel[index] })
                                }
                            }
                            GameButton {
                                Layout.fillWidth: true; Layout.fillHeight: true
                                iconLabel { text: Icons.faGamepad; color: rectangle.color }
                                text: "jogar"
                                onClicked: emiletStackView.push("PlayMenu.qml")
                            }
                        }
                        ScrollIndicator.vertical: ScrollIndicator { }
                    }
                }
            }
        }
    }
}
