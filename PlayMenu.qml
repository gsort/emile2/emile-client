import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

import br.ifba.edu.emile 1.0

import "FontAwesome"

Item {
    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins/2
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Flickable {
            Layout.fillWidth: true; Layout.fillHeight: true
            contentHeight: gridLayout.height
            clip: true
            GridLayout {
                id: gridLayout
                columns: 2; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
                width: parent.width
                Repeater {
                    model: [
                        { icon: Icons.faQuestionCircle, text: "criar uma nova partida", page: "CreateMatch.qml" },
                        { icon: Icons.faCheckCircle, text: "participar de uma partida", page: "JoinMatch.qml" }
                    ]
                    GameButton {
                        Layout.fillWidth: true; Layout.fillHeight: true
                        iconLabel { text: modelData.icon; color: rectangle.color }
                        text: modelData.text
                        onClicked: emiletStackView.push(modelData.page)
                    }
                }
            }
            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }
}
