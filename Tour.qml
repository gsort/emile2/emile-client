import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13

import "FontAwesome"

Frame {
    id: frame

    anchors { fill: parent; margins: internal.margins }
    Material.elevation: 3
    RoundButton {
        anchors { right: parent.right; top: parent.top }
        font { family: FontAwesome.solid; pointSize: 10 }
        text: Icons.faTimes
        Material.foreground: "white"
        Material.background: "#03728c"
        onClicked: {
            settings.showTour = !showTourCheckBox.checked
            frame.visible = false
        }
    }
    ColumnLayout {
        anchors { fill: parent; margins: 2*internal.margins }
        SwipeView {
            id: swipeView
            Layout.fillWidth: true; Layout.fillHeight: true; clip: true
            Repeater {
                model: [
                    { "image": "qrc:/images/customer-service-man.png",
                        "title": "Bem-vindo ao Emile!",
                        "description": "O Emile é um aplicativo móvel para gestão de atividades acadêmicas desenvolvido pelo Grupo de Pesquisa em Sistemas Distribuídos, Otimização, Redes e Tempo-Real (GSORT) do Instituto Federal de Educação, Ciência e Tecnologia da Bahia (IFBA). O Emile é atualmente utilizado por mais de 800 usuários e desenvolvido sob o formato de software livre!" },
                    { "image": "qrc:/images/graduate-female.png",
                        "title": "Funcionalidades para estudantes!",
                        "description": "Com o Emile, informações sobre sua vida acadêmica estão prontamente disponíveis na palma da sua mão. Você pode consultar o conteúdo ministrado pelos professores nas disciplinas às quais está matriculado, visualizar o registro da suas frequências e acompanhar o registro das suas notas no boletim. Além disso, você pode fazer o download dos seus documentos institucionais (comprovante de matrícula e outros) diretamente pelo Emile!" },
                    { "image": "qrc:/images/teacher-maths-female.png",
                        "title": "Funcionalidades para professores!",
                        "description": "Realize o registro das suas aulas e da frequência dos seus alunos de maneira prática e rápida. O Emile é totalmente integrado ao SUAP e todos as informações são consultadas e registradas diretamente nessa plataforma. Você pode ainda consultar os seus dados cadastrais, visualizar as notícias do portal do IFBA e os editais mais lançados pela Pró-Reitoria de Pesquisa, Pós-Graduação e Inovacão (PRPGI)!"
                    },
                    { "image": "qrc:/images/phone-new-message.png",
                        "title": "Comunicação efetiva para todos!",
                        "description": "A central de mensagens do Emile permite que professores e coordenadores de curso envie mensagens para diferentes segmentos de usuários, tais como alunos de uma turma específica ou alunos de um determinado curso. Todos os receptores envolvidos são instantaneamente notificados do envio de uma nova mensagem tornando a comunicação mais rápida e efetiva!"
                    },
                    { "image": "qrc:/images/golfer-male.png",
                        "title": "A diversão também está garantida!",
                        "description": "Que tal continuar os seus estudos de forma divertida e inovadora? O Emilet é um jogo educacional de perguntas e respostas que permite que grupos de alunos exercitem, de forma on-line, os conteúdos ministrados nas disciplinas dos diversos cursos do IFBA. Venha se divertir estudando!"
                    }
                ]
                Item {
                    ColumnLayout {
                        width: parent.width
                        spacing: 3*internal.margins
                        Image {
                            source: modelData.image
                            Layout.preferredWidth: parent.width*0.75; Layout.preferredHeight: Layout.preferredWidth; Layout.alignment: Qt.AlignHCenter
                        }
                        Label {
                            text: modelData.title
                            font { pixelSize: 25; bold: true }
                            Layout.alignment: Qt.AlignHCenter; Layout.fillWidth: true
                            horizontalAlignment: Qt.AlignHCenter
                            wrapMode: Text.WordWrap
                            color: "#1b1b1b"
                        }
                        Label {
                            text: modelData.description
                            Layout.alignment: Qt.AlignHCenter; Layout.fillWidth: true
                            horizontalAlignment: Qt.AlignHCenter
                            wrapMode: Text.WordWrap
                            color: "#1b1b1b"
                        }
                    }
                }
            }
        }
        PageIndicator {
            count: swipeView.count
            currentIndex: swipeView.currentIndex
            Layout.alignment: Qt.AlignHCenter
        }
        CheckBox { id: showTourCheckBox; text: "Não exibir mais esta apresentação"; checked: !settings.showTour }
    }
}
