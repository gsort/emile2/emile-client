#ifndef CORE_H
#define CORE_H

#include <QObject>

class NetworkController;

class Core : public QObject
{
    Q_OBJECT
    Q_PROPERTY(NetworkController * networkController READ networkController CONSTANT)
    Q_PROPERTY(QString version READ version CONSTANT)

public:
    ~Core() Q_DECL_OVERRIDE;

    static Core *instance();

    NetworkController *networkController() const;
    static QString version();

private:
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays, hicpp-avoid-c-arrays, modernize-avoid-c-arrays)
    static constexpr char _VERSION[] = "0.5.4";
    explicit Core(QObject *parent = nullptr);

    static Core *_instance;
    NetworkController *_networkController;
};

#endif // CORE_H
