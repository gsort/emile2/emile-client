package br.edu.ifba.gsort.emile;

import org.qtproject.qt5.android.bindings.QtActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import android.os.Build;
import android.os.Bundle;

import android.util.Log;

import android.app.NotificationChannel;
import android.app.NotificationManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmileActivity extends QtActivity
{
    private static final String TAG = "EmileActivity";
    private static final int RC_SIGN_IN = 123;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFirebaseToken();
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        subscribeToBasicTopics(this);
    }

    public static void subscribeToBasicTopics(Context context) {
        try {
            // Handle version topic registration
            SharedPreferences preferences = context.getSharedPreferences("pushMessagingSubscription", 0);
            if (preferences.getString("token", "").isEmpty()) {
                Log.d(TAG, "No Firebase token, returning...");
                return;
            }

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String versionTopic = preferences.getString("versionTopic", "");
            Log.d(TAG, "versionTopic from preferences: " + versionTopic);
            if (!versionTopic.equals(pInfo.versionName)) {
                Log.d(TAG, "VersionTopic " + versionTopic + " is different from " + pInfo.versionName + ", subscribing ...");
                if (!versionTopic.isEmpty()) {
                    unsubscribeFromTopic(versionTopic, context);
                }
                subscribeToTopic(pInfo.versionName, "", context);
                preferences.edit().putString("versionTopic", pInfo.versionName).apply();
            }

            // Handle 'general' topic registration
            Set<String> topics = preferences.getStringSet("topics", new HashSet<String>());
            if (!topics.contains("general")) {
                subscribeToTopic("general", "Informações gerais", context);
            } else {
                Log.d(TAG, "Already subscribed to general topic");
            }
        } catch (NameNotFoundException e) { Log.d(TAG, "NameNotFoundException"); }
    }

    private void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    String token = task.getResult().getToken();

                    // Log and toast
                    Log.d(TAG, "InstanceID Token: " + token);
                }
            }
        );
    }

    public static native void clearSettings(final String topic);

    public static void unsubscribeFromAllTopics(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("pushMessagingSubscription", 0);
        for (String topic : preferences.getStringSet("topics", new HashSet<String>())) {
            unsubscribeFromTopic(topic, context);
        }
    }

    public static void unsubscribeFromTopic(final String topic, final Context context) {
        final SharedPreferences preferences = context.getSharedPreferences("pushMessagingSubscription", 0);
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
        .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.d(TAG, "FAILED when unsubscribed from topic: " + topic);
                } else {
                    Log.d(TAG, "Successfully unsubscribed from topic: " + topic);
                    Set<String> topics = new HashSet<String>(preferences.getStringSet("topics", new HashSet<String>()));
                    topics.remove(topic);
                    preferences.edit().remove("topics").apply();
                    preferences.edit().putStringSet("topics", topics).apply();
                    Log.d(TAG, "New list of topics: " + topics);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        for (NotificationChannel channel : notificationManager.getNotificationChannels()) {
                            if (channel.getId().equals(topic)) {
                                deleteNotificationChannel(topic, context);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    public static void subscribeToTopic(final String topic, final String notificationChannelName, final Context context) {
        final SharedPreferences preferences = context.getSharedPreferences("pushMessagingSubscription", 0);
        Set<String> topics = new HashSet<String>(preferences.getStringSet("topics", new HashSet<String>()));
        Log.d(TAG, "Checking if " + topics + " contains " + topic);
        if (topics.contains(topic)) {
            Log.d(TAG, "Already subscribed to topic: " + topic + ". Aborting...");
            return;
        }
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
        .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                String msg = "Subscribed to topic: " + topic;
                if (!task.isSuccessful()) {
                    msg = "Failed to subscribe to topic: " + topic;
                } else {
                    Set<String> topics = new HashSet<String>(preferences.getStringSet("topics", new HashSet<String>()));
                    topics.add(topic);
                    preferences.edit().putStringSet("topics", topics).apply();
                    Log.d(TAG, "New list of topics: " + topics);
                    if (!notificationChannelName.isEmpty()) {
                        createNotificationChannel(topic, notificationChannelName, context);
                    }
                }
                Log.d(TAG, msg);
            }
        });
    }

    public static void createNotificationChannel(String channelId, String channelName, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            Log.d(TAG, "Notification channel " + channelId + " created!");
        }
    }

    public static void deleteNotificationChannel(String channelId, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.deleteNotificationChannel(channelId);
            Log.d(TAG, "Notification channel " + channelId + " removed!");
        }
    }

    public void startFirebashAuthUI() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            // Choose authentication providers
            List<AuthUI.IdpConfig> providers = Arrays.asList(
                    new AuthUI.IdpConfig.GoogleBuilder().build(),
                    new AuthUI.IdpConfig.FacebookBuilder().build(),
                    new AuthUI.IdpConfig.TwitterBuilder().build());

            // Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setLogo(R.drawable.emile_full)
                            .setTheme(R.style.GreenTheme)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN);
        } else {
            Log.d(TAG, "Usuário já autenticado. Usuário: " + user.getDisplayName());
            Log.d(TAG, "Usuário já autenticado. Email: " + user.getEmail());
            Log.d(TAG, "Usuário já autenticado. PhotoUrl: " + user.getPhotoUrl());
            notifyAuthentication(user.getDisplayName(), user.getEmail(), user.getPhotoUrl().toString());
        }
    }

    public static native void notifyAuthentication(final String displayName, final String email, final String photoUrl);

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Log.d(TAG, "Login realizado com sucess. Usuário: " + user.getDisplayName());
                Log.d(TAG, "Login realizado com sucess. Email: " + user.getEmail());
                Log.d(TAG, "Login realizado com sucess. PhotoUrl: " + user.getPhotoUrl());
                notifyAuthentication(user.getDisplayName(), user.getEmail(), user.getPhotoUrl().toString());
            } else {
                notifyAuthentication(null, null, null);
            }
        }
    }
}
