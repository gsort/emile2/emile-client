package br.edu.ifba.gsort.emile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import java.io.File;

public class WebViewController {
    public WebViewController(final Activity activity, final long id) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        activity.startActivity(intent);
    }

    public static void openStudentDocument(Activity activity, String fileName) {
        Uri contentUri = FileProvider.getUriForFile(activity, "br.edu.ifba.gsort.emile.fileprovider", new File(fileName));
        Intent intent = new Intent(Intent.ACTION_VIEW, contentUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        activity.startActivity(intent);
    }
}
