package br.edu.ifba.gsort.emile;

import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;

/**
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * are declared in the Manifest then the first one will be chosen.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingService";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // Force resubscription to program and courses topics
        EmileActivity.clearSettings("studentsOfProgram");
        EmileActivity.unsubscribeFromAllTopics(this);
        SharedPreferences preferences = getSharedPreferences("pushMessagingSubscription", 0);
        preferences.edit().putString("token", token).apply();
        EmileActivity.subscribeToBasicTopics(this);
    }
}
