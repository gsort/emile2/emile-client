import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import QtMultimedia 5.13

Item {
    property var match: core.networkController.context.match
    function handleMatchChanged() {
        if (match !== undefined && match.match_players.some(player => player.player_name === nicknameTextField.text))
            emiletStackView.push("ConfigureMatch.qml", { nickname: nicknameTextField.text, isCreator: true })
    }
    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: "Informe seu nickname, o nome da partida e qual Emilet você deseja jogar. Use sempre o mesmo nickname para que seus pontos sejam acumulados. Após isso, clique em RECEBER CONVIDADOS."
        }
        Label { text: "Seu nickname"; color: "white"; Layout.bottomMargin: -parent.spacing; Layout.topMargin: parent.spacing }
        TextField {id: nicknameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: matchNameLabel.font.pixelSize; Layout.bottomMargin: parent.spacing*1.5 }
        Label { id: matchNameLabel; text: "Nome da partida"; color: "white"; Layout.bottomMargin: -parent.spacing }
        TextField { id: matchNameTextField; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: matchNameLabel.font.pixelSize; wrapMode: Text.WordWrap; Layout.bottomMargin: parent.spacing*1.5 }
        Label { text: "Selecione o Emilet que você deseja jogar"; color: "white" }
        ComboBox {
            id: emiletsComboBox
            Layout.fillWidth: true
            model: core.networkController.context.emilets
            textRole: "name"
        }
        Item { Layout.fillHeight: true }
        Button {
            text: "receber convidados"
            Layout.fillWidth: true
            Material.foreground: rectangle.color
            Material.background: "#1b1b1b"
            onClicked: {
                if (nicknameTextField.text === "" || matchNameTextField.text === "") {
                    errorDialog.text = "Informe seu nickname e nome da partida antes de prosseguir!"
                    errorDialog.open()
                } else {
                    core.networkController.post(
                        "emilets/" + core.networkController.context.emilets[emiletsComboBox.currentIndex].id + "/matches/1",
                        { "description": matchNameTextField.text, "nickname": nicknameTextField.text }, "match")
                }
            }
        }
    }
    StackView.onActivated: {
        onMatchChanged.connect(handleMatchChanged)
        core.networkController.get("emilets", "emilets")
    }
    StackView.onDeactivated: {
        onMatchChanged.disconnect(handleMatchChanged)
    }
}
