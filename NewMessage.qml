import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.1

import br.ifba.edu.emile 1.0

import "FontAwesome"

Page {
    title: "Emile - Nova Mensagem"
    
    property var destinations
    property var settings

    ColumnLayout {
        z: 2
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins/2
        RowLayout {
            Layout.fillWidth: true
            spacing: internal.margins/2
            Rectangle {
                id: destinationsRectangle
                Layout.fillWidth: true; implicitHeight: childrenRect.height
                color: "#1b1b1b"; opacity: 0.85; radius: 4
                ColumnLayout {
                    width: parent.width
                    spacing: 0
                    Label { padding: titleFrame.padding; bottomPadding: 0; color: "white"; Layout.fillWidth: true; font.bold: true; text: "Destinations:" }
                    Repeater {
                        model: destinations
                        Label { id: destinationsLabel; padding: titleFrame.padding; bottomPadding: (index !== destinations.length-1) ? 0:titleFraming.padding; topPadding: 0; text: "\u2022 " + destinations[index].description; color: "white"; Layout.maximumWidth: parent.width; elide: Text.ElideRight }
                    }
                }
            }
            ToolButton {
                id: avatarToolButton
                Layout.preferredWidth: Math.min(64, destinationsRectangle.height); Layout.preferredHeight: Layout.preferredWidth
                Layout.alignment: Qt.AlignVCenter
                padding: 0
                Image {
                    id: messageUserAvatar
                    anchors.fill: parent
                    source: settings.avatar
                    layer.enabled: !source.toString().startsWith('qrc:/')
                    layer.effect: OpacityMask {
                        maskSource: Rectangle { width: messageUserAvatar.width; height: width; radius: width/2; visible: false }
                    }
                }
                onClicked: stackView.push("qrc:/EmileSettings.qml", { settings: settings, autoOpen: true })
            }
        }
        Frame {
            id: titleFrame
            Layout.fillWidth: true
            Material.elevation: 3
            TextField { id: titleTextField; anchors.fill: parent; placeholderText: "Assunto"; wrapMode: Text.WordWrap }
        }
        Frame {
            Layout.fillWidth: true; Layout.fillHeight: true
            topPadding: padding; bottomPadding: padding
            Material.elevation: 3
            TextArea { id: bodyTextField; anchors.fill: parent; placeholderText: "Mensagem"; wrapMode: Text.WordWrap }
        }
        Button {
            Layout.fillWidth: true; text: "Enviar"
            Material.foreground: "white"
            Material.background: "#03728c"
            onClicked: {
                if (titleTextField.text === "" || bodyTextField.text === "") errorDialog2.open()
                else confirmationDialog.open()
            }
        }
    }
    Dialog {
        id: errorDialog2
        title: "Erro"
        x: parent.width/2-width/2
        y: parent.height/2-height/2
        width: parent.width - 4*internal.margins
        Label { text: "Você deve informar o assunto e o corpo da mensagem!"; width: parent.width; wrapMode: Text.WordWrap }
        standardButtons: Dialog.Ok
    }
    Dialog {
        id: confirmationDialog
        title: "Envio de mensagem"
        x: parent.width/2-width/2
        y: parent.height/2-height/2
        width: parent.width - 4*internal.margins
        Label { text: "Você confirma o envio desta mensagem?"; width: parent.width; wrapMode: Text.WordWrap }
        standardButtons: Dialog.Yes | Dialog.No
        onAccepted: {
            for (var i = 0; i < destinations.length; ++i) {
                if (destinations[i].checked) {
                    core.networkController.post("messages/", {
                                                    topic: destinations[i].name,
                                                    topic_name: destinations[i].description,
                                                    sender_name: core.networkController.username,
                                                    sender_avatar: settings.avatar,
                                                    title: titleTextField.text,
                                                    body: bodyTextField.text
                                                })
                }
            }
            stackView.pop(); stackView.pop()
        }
    }
    Image {
        anchors.fill: parent
        source: "qrc:/images/background.png"
        fillMode: Image.PreserveAspectCrop
    }
}
