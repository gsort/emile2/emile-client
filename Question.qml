import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13

import "FontAwesome"

Item {
    property var question
    property bool update
    property bool approve: false
    property bool dirty: false
    property string resourceName: "questions"

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins

        Label {
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
            color: "white"; text: "Emilet!"
        }
        Label {
            Layout.fillWidth: true; wrapMode: Text.WordWrap; color: "white"
            horizontalAlignment: Qt.AlignHCenter
            text: "Informe os dados - enunciado e resposta(s) - da questão. Para questões abertas informe somente a resposta correta. Para questões de múltipla escolha informe as diversas opções e selecione a resposta correta."
        }
        Label { text: "Nome"; color: "white"; Layout.bottomMargin: -parent.spacing; Layout.topMargin: parent.spacing }
        TextField {id: nameTextField; text: question.name; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: descriptionLabel.font.pixelSize; onDisplayTextChanged: if (question.name !== nameTextField.displayText) { dirty = true; question.name = nameTextField.displayText } Layout.bottomMargin: parent.spacing*1.5 }
        Label { id: descriptionLabel; text: "Descrição"; color: "white"; Layout.bottomMargin: -parent.spacing }
        TextField { id: descriptionTextField; text: question.description; Layout.fillWidth: true; Material.foreground: "white"; font.pixelSize: descriptionLabel.font.pixelSize; wrapMode: Text.WordWrap; onDisplayTextChanged: if (question.description !== descriptionTextField.displayText) { dirty = true; question.description = descriptionTextField.displayText } Layout.bottomMargin: parent.spacing*1.5 }
        Label { text: "Opções"; color: "white" }
        ButtonGroup { id: buttonGroup }
        ListView {
            id: listView
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2; clip: true
            model: question.question_options
            delegate: Frame {
                width: parent.width
                padding: 0; topPadding: 0; bottomPadding: 0
                Material.elevation: 3; Material.background: "white"
                SwipeDelegate {
                    anchors.fill: parent
                    text: modelData.description; Material.foreground: "#1b1b1b"
                    leftPadding: 40
                    RadioButton {
                        anchors { left: parent.contentItem.left; leftMargin: -parent.leftPadding; verticalCenter: parent.contentItem.verticalCenter } ButtonGroup.group: buttonGroup; checked: question.right_option === index
                        onCheckedChanged: if (checked && question.right_option !== index) { question.right_option = index; dirty = true }
                    }
                    swipe.left: Label {
                        id: deleteLabel
                        text: "Remover"; color: "white"
                        verticalAlignment: Label.AlignVCenter
                        padding: 12
                        height: parent.height
                        anchors.left: parent.left
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if (question.right_option >= index && question.right_option > 0)
                                    question.right_option--
                                question.question_options.splice(index, 1)
                                dirty = true
                                listView.model = question.question_options
                            }
                        }
                        background: Rectangle { color: deleteLabel.SwipeDelegate.pressed ? Qt.darker("tomato", 1.1) : "tomato" }
                    }
                }
            }
            populate: Transition { NumberAnimation { properties: "x, y"; duration: 2000; easing.type: Easing.OutElastic } }
        }
    }
    Dialog {
        id: addOption
        width: parent.width - $*internal.margins
        title: "Nova Opção"
        standardButtons: Dialog.Ok | Dialog.Cancel
        x: parent.width/2 - width/2; y: parent.height/2 - height/2
        TextField { id: textField; placeholderText: "Digite a nova opção"; width: parent.width }
        onAccepted: {
            question.question_options.push({ description: textField.text })
            if (question.right_option === -1)
                question.right_option = 0
            listView.model = question.question_options
            dirty = true
        }
        onVisibleChanged: { textField.clear(); textField.forceActiveFocus() }
    }
    RoundButton {
        anchors { bottom: parent.bottom; right: parent.right; margins: 15 }
        font { family: FontAwesome.solid; pointSize: 16 }
        padding: 20
        text: Icons.faPlus
        Material.foreground: rectangle.color; Material.background: "#1b1b1b"
        onClicked: addOption.open()
    }
    Dialog {
        id: dirtyDialog
        width: parent.width - 4*internal.margins
        title: "Questão Modificada"
        standardButtons: Dialog.Yes | Dialog.No | Dialog.Discard
        x: parent.width/2 - width/2; y: parent.height/2 - height/2
        contentItem: Label { text: "Você realizou modificações nesta questão. Deseja salvá-la?"; wrapMode: Text.WordWrap }
        onAccepted: save()
        onRejected: emiletStackView.pop()
        onDiscarded: close()
    }
    function save() {
        if (update) {
            question.approved = approve
            core.networkController.put("categories/" + question.category_id + "/questions/" + question.id + "/0", question, resourceName)
        } else {
            core.networkController.post("categories/" + question.category_id + "/questions/0", question, "questions")
        }
        emiletStackView.pop()
    }
    Connections {
        target: saveButton
        function onClicked() { save() }
    }
    Connections {
        target: backButton
        function onClicked() { if (dirty) dirtyDialog.open() }
    }
    StackView.onActivated: saveButton.visible = Qt.binding(function() { return dirty || approve })
    StackView.onRemoved: { saveButton.visible = false }
}
