import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1

import "FontAwesome"

Page {
    title: "Emile - SEI - Detalhes do Processo"

    property var settings
    property var processCache
    property int documentsCount
    property int movesCount

    Flickable {
        anchors { fill: parent; margins: internal.margins }
        contentWidth: parent.width - 2*anchors.margins
        contentHeight: columnLayout.height

        ColumnLayout {
            id: columnLayout
            width: parent.width - 2*parent.anchors.margins
            spacing: 2*internal.margins
            GroupBox {
                title: "Autuação"
                Layout.fillWidth: true
                Material.elevation: 3
                ColumnLayout {
                    width: parent.width
                    spacing: internal.margins
                    IconLabel { icon: Icons.faCubes; text: core.networkController.seiProcessDetails.data['Processo'] }
                    IconLabel { icon: Icons.faMapSigns; text: core.networkController.seiProcessDetails.data['Tipo'] }
                    IconLabel { icon: Icons.faCalendar; text: core.networkController.seiProcessDetails.data['Data de Registro'] }
                    IconLabel { icon: Icons.faUser; text: "Interessados:\n" + core.networkController.seiProcessDetails.data['Interessados'] }
                }
            }
            GroupBox {
                title: "Protocolos (" + core.networkController.seiProcessDetails.documents.length + " registros" + ((core.networkController.seiProcessDetails.documents.length !== documentsCount) ? (" - " + (core.networkController.seiProcessDetails.documents.length-documentsCount) + ((core.networkController.seiProcessDetails.documents.length-documentsCount === 1) ? " novo":" novos")):"") + ")"
                Layout.fillWidth: true
                Material.elevation: 3
                ColumnLayout {
                    width: parent.width
                    spacing: internal.margins
                    Repeater {
                        model: core.networkController.seiProcessDetails.documents
                        IconLabel { icon: Icons.faFileAlt; text: modelData.type + " (" + modelData.department + " - " + modelData.date + ")"; font.bold: (index >= documentsCount) ? true:false }
                    }
                }
            }
            GroupBox {
                title: "Andamentos (" + core.networkController.seiProcessDetails.moves.length + " registros" + ((core.networkController.seiProcessDetails.moves.length !== movesCount) ? (" - " + (core.networkController.seiProcessDetails.moves.length-movesCount) + ((core.networkController.seiProcessDetails.moves.length-movesCount === 1) ? " novo":" novos")):"") + ")"
                Layout.fillWidth: true
                Material.elevation: 3
                ColumnLayout {
                    width: parent.width
                    spacing: internal.margins
                    Repeater {
                        model: core.networkController.seiProcessDetails.moves
                        IconLabel { icon: Icons.faShoePrints; text: modelData.datetime + " (" + modelData.department + ") - " + modelData.description; font.bold: (index < core.networkController.seiProcessDetails.moves.length-movesCount) ? true:false }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        processCache = settings.value("processCache")
        if (processCache === undefined) {
            processCache = ({})
        }
        if (processCache[core.networkController.seiProcessDetails.data['Processo']] === undefined) {
            documentsCount = core.networkController.seiProcessDetails.documents.length
            movesCount = core.networkController.seiProcessDetails.moves.length
        } else {
            documentsCount = processCache[core.networkController.seiProcessDetails.data['Processo']].documentsCount
            movesCount = processCache[core.networkController.seiProcessDetails.data['Processo']].movesCount
        }
        processCache[core.networkController.seiProcessDetails.data['Processo']] = {}
        processCache[core.networkController.seiProcessDetails.data['Processo']].documentsCount = core.networkController.seiProcessDetails.documents.length
        processCache[core.networkController.seiProcessDetails.data['Processo']].movesCount = core.networkController.seiProcessDetails.moves.length
        settings.setValue("processCache", processCache)
        settings.sync()
    }
}
