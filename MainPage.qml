import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1

import "FontAwesome"

Page {
    title: "Bem-vindo ao Emile"

    property var settings

    ListView {
        id: mainListView
        anchors { fill: parent; margins: internal.margins }
        model: [
            { "icon": Icons.faBook, "description": "funcionalidades do SUAP", "page": "qrc:/SUAP.qml" },
            { "icon": Icons.faLaptopCode, "description": "SEI - consulta pública", "page": "qrc:/SEIPublicSearch.qml" },
            { "icon": Icons.faRss, "description": "notícias do IFBA", "page": "qrc:/IFBANews.qml" },
            { "icon": Icons.faHandshake, "description": "editais 2019 da PRPGI", "page": "qrc:/PRPGINotices.qml" },
            { "icon": Icons.faEnvelope, "description": "central de mensagens", "page": "qrc:/Messages.qml" },
            { "icon": Icons.faGamepad, "description": "emilet", "page": "qrc:/Emilet.qml" },
            { "icon": Icons.faCogs, "description": "configurações", "page": "qrc:/EmileSettings.qml" }
        ]
        spacing: internal.margins/2
        delegate: Item {
            width: mainListView.width
            height: frame.height
            Frame {
                id: frame
                width: parent.width
                Material.elevation: 3
                ColumnLayout {
                    id: columnLayout
                    width: parent.width
                    spacing: internal.margins
                    Label {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        font { family: FontAwesome.solid; pointSize: 24 }
                        text: modelData.icon
                        color: "#03728c"
                    }
                    Label {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        font.capitalization: Font.AllUppercase
                        text: modelData.description
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    switch(index) {
                        case 0:
                            core.networkController.getBasicData()
                            break
                        case 1:
                            core.networkController.resetSEIPublicSearchAttempts()
                            core.networkController.prepareSEIPublicSearch()
                            break
                        case 5:
                            core.networkController.startFirebashAuthUI()
                            break
                        default:
                            stackView.push(modelData.page, { settings: settings })
                            break
                    }
                }
            }
        }
    }

    Connections {
        target: core.networkController
        function onUserAuthenticated() { stackView.push("qrc:/SUAP.qml", { settings: settings }) }
        function onSeiPublicSearchPrepared() { stackView.push("qrc:/SEIPublicSearch.qml", { settings: settings }) }
        function onFirebaseUserAuthenticated(displayName, email, photoUrl) {
            if (displayName !== "") {
                settings.displayName = displayName
                settings.email = email
                settings.photoUrl = photoUrl
                stackView.push("qrc:/EmiletMain.qml", { settings: settings })
            } else {
                console.log("Erro de autenticação")
            }
        }
    }
}
